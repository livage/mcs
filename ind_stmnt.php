<?php 

include_once "db.php"; 
//$id=$_GET['id'];
$sqlfilter="";
//if($_GET['filter']!="")
//$sqlfilter=" and id =".$_GET['filter'];
$company = "Belvedere Medical Centre";
$address = "189 Samora Machel Avenue";
$email = "bmc@gmail.com";
$telephone ="04-740185";


require('invoice/u/fpdf.php');

class PDF extends FPDF
{
function Header()
{

$this->Image("invoice/logo/logo.jpg",10,10,20);
$this->SetFont('Arial','B',12);
$this->Ln(9);

}
function Footer()
{
$this->SetY(-15);
$this->SetFont('Arial','I',8);
$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',12);
$pdf->SetTextColor(32);
$pdf->Cell(0,5,$company,0,1,'R');
$pdf->Cell(0,5,$address,0,1,'R');
$pdf->Cell(0,5,$email,0,1,'R');
$pdf->Cell(0,5,'Tel: '.$telephone,0,1,'R');
$pdf->Cell(0,10,'',0,1,'R');
$pdf->SetFillColor(200,220,255);

//$pdf->Cell(0,10,'',0,1,'R');
//$pdf->SetDrawColor(192,192,192);



$id=$_GET['sid'];	
if($_GET['nature']=="I")
	{
	$sql="SELECT * FROM persons p INNER JOIN accounts ON p.id=accounts.owner  WHERE nature='I' ";

	}
elseif($_GET['nature']=="C"){

$sql="SELECT * FROM corporate p INNER JOIN accounts ON p.id=accounts.owner  WHERE nature='C'";

}
				
$line = $db->queryUniqueObject($sql." AND p.id=$id");
if($_GET['nature']=="I")
	{
if($line->role =="M")
	{
		$sql="SELECT * ,persons.id as pid FROM persons INNER JOIN accounts ON persons.id=OWNER AND nature= 'I' INNER JOIN corporate_members ON memberId=persons.id INNER JOIN corporate ON corparateId=corporate.id WHERE  persons.id=$id";
				
				//echo $sql;
		$line = $db->queryUniqueObject($sql);
	}
	}
       
	  if ($line->nature!="C") {
		  $pdf->Cell(50,7,'Category :',1,0,'C');
			$pdf->Cell(50,7,$line->category,1,1,'C');
	    } 
		
		$label="";
		$value="";
		if ($line->nature=="C") {
			$label= "Corporate Number:"; 
			$value=$line->cor_number;
		}else { 
		$label= "Patient Number:"; 
		$value=$line->pat_number;
		} 
		 $pdf->Cell(50,7,$label,1,0,'C');
			$pdf->Cell(50,7,$value,1,1,'C');
        
		
		 $pdf->Cell(50,7,'Account Number:',1,0,'C');
			$pdf->Cell(50,7,$line->acc_number,1,1,'C');
		
		 $pdf->Cell(50,7,'Status:',1,0,'C');
			$pdf->Cell(50,7,$line->status,1,1,'C');
	
	$cname="";
	 if ($line->nature=="C") $cname=  $line->c_name; else $cname= $line->firstname ." ".$line->surname; 
	$pdf->Cell(50,7,'Contact person:',1,0,'C');
			$pdf->Cell(50,7,$cname,1,1,'C');
			
			
        
 if ($line->nature=="C"  ){
if($line->name!= ""){	 
        $pdf->Cell(50,7,'Corporate Name:',1,0,'C');
			$pdf->Cell(50,7,$line->name,1,1,'C');

} }
		  
		  $pdf->Cell(50,7,'Balance:',1,0,'C');
			$pdf->Cell(50,7,"$".number_format($line->balance,2),1,1,'C');
      
      
      $pdf->Ln();  
 
$sql="";

	if($_GET['nature']=="C"){   
	 $sql= " SELECT  * ,DATE_FORMAT( insTS,'%d/%m/%Y' ) as dt FROM transactions WHERE account =  ".$line->acc_number;
 //$query= " SELECT  COUNT(*) as num FROM transactions WHERE account =  ".$line->acc_number." OR account= (SELECT acc_number FROM accounts AS acc INNER JOIN corporate_members ON acc.owner=`memberId` WHERE `corparateId`=$id AND nature='I')";
//echo $sql;
 }
 else if($_GET['nature']=="I"){
$sql="SELECT  * ,DATE_FORMAT( insTS,'%d/%m/%Y' ) as dt  FROM transactions WHERE account = ".$line->acc_number;
}









$pdf->Cell(185,7,"Account Transactions ",1,1,'C');
$pdf->Cell(185,7,"",1,1,'C');

$pdf->Cell(35,7,'Date',1,0,'C');
$pdf->Cell(60,7,'Reference',1,0,'C');
$pdf->Cell(30,7,'Account',1,0,'C');
$pdf->Cell(30,7,'Description',1,0,'C');
$pdf->Cell(30,7,'Total Bill',1,1,'C');


$results = mysql_query($sql);
		  	while($row = mysql_fetch_array($results))
			{

		

$pdf->Cell(35,7, $row['dt'],1,0,'C');
$pdf->Cell(60,7,$row['ref'],1,0,'C');
$pdf->Cell(30,7,$row['account'],1,0,'C');
$trans="";
if($row['type']=='L')$trans= "Loan"; else if($row['type']=='D')$trans="Deposit"; else if($row['type']=='W')$trans="Visit";else if($row['type']=='R')$trans= "Reversal";
$pdf->Cell(30,7,$trans,1,0,'C');

/*
$x = $pdf->GetX();
$y1 = $pdf->GetY();

$pdf->MultiCell(110,7,$row['description'],1,'L',false);
$y2 = $pdf->GetY();
$pdf->SetXY($x + 110, $y1);

$height= $y2-$y1;
*/
$pdf->Cell(30,7,"$".number_format($row['amount'],2),1,1,'C');


}
$pdf->Cell(185,7,"",1,1,'C');

  
if($_GET['nature']=="C"){
	$pdf->Ln();
											
$pdf->Cell(185,7,"Member Transactions count",1,1,'C');
$pdf->Cell(185,7,"",1,1,'C');
							
$pdf->Cell(100,7,'Member',1,0,'C');
$pdf->Cell(85,7,'Count of transaction',1,1,'C');							

      			
$csql="SELECT acc_number, acc.owner as owner FROM accounts AS acc INNER JOIN corporate_members ON acc.owner=`memberId` WHERE `corparateId`=$id AND nature='I'";
 $accounts=mysql_query($csql);
 while($rowa = mysql_fetch_array($accounts)){
	$resultb = mysql_query("SELECT  count(*) num FROM transactions WHERE account = ".$rowa["acc_number"]);
	while($row = mysql_fetch_array($resultb))
		{

		/* $mysqldate=$row['date'];

 		$phpdate = strtotime( $mysqldate );

 		$phpdate = date("d/m/Y",$phpdate);

			*/							 
	$pdf->Cell(100,7,$rowa["acc_number"],1,0,'C');
	$pdf->Cell(85,7, $row["num"],1,1,'C');							 
										 
		
 }
 }
}
$pdf->Ln();
//$pdf->Cell(190,40,$com,0,0,'C');
$filename="invoice.pdf";
$pdf->Output();

?>