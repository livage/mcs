<?php
session_start(); // Use session variable on this page. This function must put on the top of page.

if(!isset($_SESSION['username']) ){ // if session variable "username" does not exist.
header("location:login.php?msg=Please%20login%20to%20access%20admin%20area%20!"); // Re-direct to index.php
}
elseif (isset($_SESSION['username']) && ($_SESSION['usertype'] =='Admin' || $_SESSION['usertype'] =='Supervisor' || $_SESSION['usertype'] =='User' || $_SESSION['usertype'] =='Patient'))
{
	include_once "db.php"; 
	error_reporting (E_ALL ^ E_NOTICE);


?>
<!DOCTYPE HTML>
<html>
<head>
<title>BMC Mobile App</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<style type="text/css">
body,td,th {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #FFFFFF;
}
</style>
</head>
<body>
   <div class="header">	
    <div class="header-top">
       <div class="wrap"> 
	         <div class="logo">
				<a href="index.html"><img src="images/logo.png" alt="" /></a>
			 </div>
			 <div class="cssmenu"> </div>
		    <div class="clear"></div>
	   </div>
	 </div>
	        <div class="header-bottom" id="section-1">
				<div class="wrap"></div>
  			</div>
 		</div>
   <!-- End Main -->
	   <!-- Footer -->
       
         <div class="footer" id="section-5">
    	   <div id="content">
	 <?php
	 $id=$_GET['id'];
	 $sql="SELECT * FROM patient_info WHERE id=".$_GET['id'];
			$line= $db->queryUniqueObject($sql);
	 ?>
	 
      <h1> Patient Informatics</h1>
	  
	 <form action="" method="post">
       <table width="300"  border="0" cellspacing="0" cellpadding="0">
	     <tr>
           <td width="155">Date:</td>
           <td width="473"><?php echo $line->date;; ?>
		  
		   </td>
         </tr>
	    <tr>
           <td width="155">Account:</td>
           <td width="473"><?php echo $line->acc_num; ?>
		   
		   </td>
         </tr>
		  <tr>
           <td width="155">Patient:</td>
           <td width="473"><?php 
		   
		   $patie=$db->queryUniqueObject("SELECT * FROM persons  WHERE  persons.id= ".$line->patient );
		   echo $patie->firstname." ".$patie->surname; ?>
		   <input type="hidden" name="patient" value="<?php echo $_GET['pid']; ?> " />
		   </td>
         </tr>
	
         <tr>
           <td width="155">Weight:</td>
           <td width="473"><?php echo $line->weight; ?></td>
         </tr> 
       
         <tr>
           <td width="155">Height:</td>
           <td width="473"><?php echo $line->height; ?></td>
         </tr>  
		 
         <tr>
           <td width="155">Glucose Level:</td>
           <td width="473"><?php echo $line->glucose; ?></td>
         </tr>
		  <tr>
           <td width="155">CD4 Count:</td>
           <td width="473"><?php echo $line->cd4; ?></td>
         </tr>
		  <tr>
		 <td width="155">BMI:</td>
           <td width="473"><?php echo $line->bmi; ?></td>
         </tr>
		  <tr>
       		  <td width="155">Attending Doctor:
           </td>
           <td width="473">
		   <?php echo $line->doctor; ?>
		  
		   
		   </td>
         </tr>
      		  <tr>
       		  <td width="155">Doctor's Notes:
           </td>
           <td width="473">
		   <?php echo $line->notes; ?>
		  
		   
		   </td>
         </tr>
       
         <tr>
           <td align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
           <td align="left">&nbsp;&nbsp;</td>
         </tr>
       </table>
       
     </form>
     <div align="justify"></div>
<div id="respond"></div>
    </div>    
          <div class="footer-bottom">
            <div class="copy">
		      <p> © All Rights Reserved 2014 BMC</p>
	       </div>	    
	     </div>   
     </div>
  </body>
</html>
<?php } ?>
    	
    	
            