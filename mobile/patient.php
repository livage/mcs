<?php
session_start(); // Use session variable on this page. This function must put on the top of page.

if(!isset($_SESSION['username']) ){ // if session variable "username" does not exist.
header("location:login.php?msg=Please%20login%20to%20access%20admin%20area%20!"); // Re-direct to index.php
}
elseif (isset($_SESSION['username']) && ($_SESSION['usertype'] =='Admin' || $_SESSION['usertype'] =='Supervisor' || $_SESSION['usertype'] =='User' || $_SESSION['usertype'] =='Patient'))
{
	include_once "db.php"; 
	error_reporting (E_ALL ^ E_NOTICE);


?>
<!DOCTYPE HTML>
<html>
<head>
<title>BMC Mobile App</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<style type="text/css">
body,td,th {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #FFFFFF;
}
</style>
</head>
<body>
   <div class="header">	
    <div class="header-top">
       <div class="wrap"> 
	         <div class="logo">
				<a href="index.html"><img src="images/logo.png" alt="" /></a>
			 </div>
			 <div class="cssmenu"> </div>
		    <div class="clear"></div>
	   </div>
	 </div>
	        <div class="header-bottom" id="section-1">
				<div class="wrap"></div>
  			</div>
 		</div>
   <!-- End Main -->
	   <!-- Footer -->
       
         <div class="footer" id="section-5">
    	   <div class="wrap">
              <div class="footer-top">
                <div class="section group">
				<div class="col_1_of_3 span_1_of_3">
<h3>PATIENT INFORMATICS:- Select the patient for PI readings</h3>
<form   id="accountform" class ="accountNo" method="post" action="pi_admin.php?c=<?php echo "pi_admin&app=".$_GET['app']; ?>" >
<!--[if IE]><input type="text" style="display: none;" disabled="disabled" size="1" /><![endif]-->

 <input type="hidden" name="accountNo" value="<?php echo $_POST["accountNo"];?>"/>
 
 <?php 
if(isset($_POST["accountNo"]))
$line=$db->queryUniqueObject("SELECT * FROM persons INNER JOIN accounts ON persons.id=OWNER WHERE nature='I' AND acc_number =".$_POST["accountNo"]);
else
$line=$db->queryUniqueObject("SELECT * FROM persons INNER JOIN accounts ON persons.id=OWNER WHERE nature='I' AND acc_number =".$_GET["acc"]);
?>
 
<table class="myTableStyle" >
	 
	<tr>
           <td width="155">Patient:</td>
           
           <td><label for="patientId"></label>
             <select name="patientId" id="patientId">
			
               <option value="<?php echo $line->id;?>">Self</option>
			   <?php
			   
			   $result = mysql_query("SELECT * FROM dependant INNER JOIN persons ON dependent=persons.id WHERE holder=".$line->id);
		  	while($row = mysql_fetch_array($result))
			{
			   ?>
               <option value="<?php echo $row['id']; ?>"><?php echo $row['firstname']." ".$row['surname']; ?></option>
			   <?php } ?>
            </select></td>
			<td>&nbsp;&nbsp;&nbsp;
             <input type="submit" name="Submit" value="Save" /></td>
         </tr>
</table>

</form></p>
                  <p>&nbsp;</p>
				</div>
                </div>
            </div> 
         </div>    
          <div class="footer-bottom">
            <div class="copy">
		      <p> © All Rights Reserved 2014 BMC</p>
	       </div>	    
	     </div>   
     </div>
  </body>
</html>
<?php } ?>
    	
    	
            