/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.1.33-community : Database - bmc2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bmc2` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `bmc2`;

/*Table structure for table `_user` */

DROP TABLE IF EXISTS `_user`;

CREATE TABLE `_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `username` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `branch` bigint(20) DEFAULT NULL,
  `active` binary(1) DEFAULT '1',
  `personId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `_user` */

insert  into `_user`(`id`,`firstname`,`surname`,`username`,`password`,`user_type`,`branch`,`active`,`personId`) values (1,'Admin','Admin','admin','admin','Admin',1,'1',NULL);

/*Table structure for table `accounts` */

DROP TABLE IF EXISTS `accounts`;

CREATE TABLE `accounts` (
  `acc_number` bigint(10) unsigned zerofill NOT NULL,
  `category` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `balance` double DEFAULT NULL,
  `pin_number` varchar(4) DEFAULT NULL,
  `owner` bigint(20) DEFAULT NULL,
  `nature` varchar(1) DEFAULT NULL COMMENT 'C= corparate, I=individual',
  `overdraft` double DEFAULT NULL,
  `insTs` timestamp NULL DEFAULT NULL,
  `insUs` varchar(255) DEFAULT NULL,
  `updTs` timestamp NULL DEFAULT NULL,
  `updUs` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`acc_number`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `accounts` */

insert  into `accounts`(`acc_number`,`category`,`status`,`balance`,`pin_number`,`owner`,`nature`,`overdraft`,`insTs`,`insUs`,`updTs`,`updUs`) values (0000000001,'Loan','Active',NULL,'0000',1,'G',NULL,NULL,NULL,NULL,NULL),(0000000002,'Bmc','Active',NULL,'0000',2,'G',NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `bill` */

DROP TABLE IF EXISTS `bill`;

CREATE TABLE `bill` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `patient` bigint(20) DEFAULT NULL,
  `date_of_sevice` datetime DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL COMMENT 'paid, outstanding',
  `method_of_payment` varchar(50) DEFAULT NULL COMMENT 'cash, account, zimswitch',
  `total_bill` double DEFAULT NULL,
  `branch` bigint(20) DEFAULT NULL,
  `acc_owner` bigint(20) DEFAULT NULL,
  `trans_fee` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `bill` */

/*Table structure for table `branch` */

DROP TABLE IF EXISTS `branch`;

CREATE TABLE `branch` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(8) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `active` binary(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `branch` */

insert  into `branch`(`id`,`code`,`name`,`address`,`active`) values (1,'BMC0001','Loan Account','','1'),(2,'BMC0002','BMC ','','1');

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `catergory` varchar(6) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `value` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `categories` */

insert  into `categories`(`catergory`,`name`,`value`) values ('medcat','Platinum','Platinum'),('medcat','Gold','Gold'),('medcat','Silver','Sliver'),('medcat','Bronze','Bronze'),('usrtyp','Administrator','Admin'),('usrtyp','User','User'),('gender','Male','M'),('gender','Female','F'),('usrtyp','Supervisor','Supervisor'),('accsts','Pending','Pending'),('accsts','Active','Active'),('accsts','Suspended','Suspended'),('accsts','Closed','Closed');

/*Table structure for table `corporate` */

DROP TABLE IF EXISTS `corporate`;

CREATE TABLE `corporate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `reg_num` varchar(255) DEFAULT NULL,
  `inception_date` date DEFAULT NULL,
  `c_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `phone` varchar(56) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `cor_number` varchar(255) DEFAULT NULL,
  `insUs` varchar(255) DEFAULT NULL,
  `insTs` datetime DEFAULT NULL,
  `updUs` varchar(255) DEFAULT NULL,
  `updTs` date DEFAULT NULL,
  `active` binary(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `corporate` */

/*Table structure for table `corporate_members` */

DROP TABLE IF EXISTS `corporate_members`;

CREATE TABLE `corporate_members` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `corparateId` bigint(20) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `joinDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insU` varchar(255) DEFAULT NULL,
  `insTS` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updU` varchar(255) DEFAULT NULL,
  `updTS` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` binary(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `corporate_members` */

/*Table structure for table `dependant` */

DROP TABLE IF EXISTS `dependant`;

CREATE TABLE `dependant` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `holder` bigint(20) DEFAULT NULL,
  `dependent` bigint(20) DEFAULT NULL,
  `relationship` varchar(50) DEFAULT NULL,
  `active` binary(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `dependant` */

/*Table structure for table `persons` */

DROP TABLE IF EXISTS `persons`;

CREATE TABLE `persons` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `id_number` varchar(17) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `phone` varchar(56) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pat_number` varchar(255) DEFAULT NULL,
  `role` varchar(1) DEFAULT NULL COMMENT 'P=patient, M=memember, D= dependent,C=coprate,W= walkin',
  `image` varchar(4) DEFAULT NULL COMMENT 'file extsion (jepg,jpg, gif)',
  `insUs` varchar(255) DEFAULT NULL,
  `insTs` datetime DEFAULT NULL,
  `updUs` varchar(255) DEFAULT NULL,
  `updTs` date DEFAULT NULL,
  `active` binary(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `persons` */

/*Table structure for table `products_services` */

DROP TABLE IF EXISTS `products_services`;

CREATE TABLE `products_services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(6) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `category` bigint(20) DEFAULT NULL,
  `branch` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

/*Data for the table `products_services` */

insert  into `products_services`(`id`,`code`,`description`,`price`,`category`,`branch`) values (1,'MS004','JADELLE',60,NULL,NULL),(2,'MS005','SPLINTING',10,NULL,NULL),(3,'MS006','URINALYSIS',10,NULL,NULL),(4,'MS007','RDT MALARIA TEST',10,NULL,NULL),(5,'MS008','SUTURE REMOVAL',5,NULL,NULL),(6,'MS009','ADMIISSION  RESUS INCLUDING 1 VACOLITRE',110,NULL,NULL),(7,'MS010','BP CHECK',1,NULL,NULL),(8,'MS011','DEBRIDEMENT UNDER LOCAL',50,NULL,NULL),(9,'MS012','COMPLICATED DEBRIDEMENT',70,NULL,NULL),(10,'MS013','ECG',50,NULL,NULL),(11,'MS014','BACKSLAB MAX 4POP & 2ORTHOWOOL',70,NULL,NULL),(12,'MS015','REMOVAL OF SUBCUT FB',50,NULL,NULL),(13,'MS016','ENEMA',120,NULL,NULL),(14,'MS017','IM + IV INJECTION',25,NULL,NULL),(15,'MS018','EYE IRRIGATION',50,NULL,NULL),(16,'MS019','INJECTION OUTPATIENT ',15,NULL,NULL),(17,'MS020','GLUCOMETER',10,NULL,NULL),(18,'MS021','NGT',5,NULL,NULL),(19,'MS022','NASAL PACKING',120,NULL,NULL),(20,'MS023','N/S SCREEN',10,NULL,NULL),(21,'MS024','YELLOW FEVER',70,NULL,NULL),(22,'MS025','ANTIRABIES',50,NULL,NULL),(23,'MS026','CERVICAL COLLAR',25,NULL,NULL),(24,'MS027','PAP SMEAR',60,NULL,NULL),(25,'MS028','FOREIGN BODY EASILY ACCESSIBLE',30,NULL,NULL),(26,'MS029','ASCITIC TAPING',100,NULL,NULL),(27,'MS030','MEDICAL EXAM ONLY',55,NULL,NULL),(28,'MS031','MEDICAL EXAM + X-RAY IMMIGRATION',130,NULL,NULL),(29,'MS032','X-RAY ONLY IMMIGRATION',100,NULL,NULL),(30,'MS033','ECG DOTS (EACH)',1,NULL,NULL),(31,'MS034','IV DRESSING /TEGADERM',2,NULL,NULL),(32,'MS035','PRIMAPORE DRESSING',3,NULL,NULL),(33,'MS036','LEUKOMED T PLUS',1,NULL,NULL),(34,'MS037','OPSITE FLEXIGRID',1,NULL,NULL),(35,'MS038','LEUKOSTRIP',3,NULL,NULL),(36,'MS039','Consultation ',30,NULL,NULL),(37,'MS040','Admission  OBS',55,NULL,NULL),(38,'MS041','NEBULISATION PER CYCLE  × 1',15,NULL,NULL),(39,'MS042','BLOOD COLLECTION',10,NULL,NULL),(40,'MS043','PREG TEST',5,NULL,NULL),(41,'MS044','IV & IM OWN MEDICINE',5,NULL,NULL),(42,'MS045','SIMPLE DRESSING',12,NULL,NULL),(43,'MS046','LARGE DRESSING',15,NULL,NULL),(44,'MS047','SUTURE MINOR           INCL ATT',50,NULL,NULL),(45,'MS048','SUTURE MAJOR/MULTIPLE WOUNDS',130,NULL,NULL),(46,'MS049','I & D',40,NULL,NULL),(47,'MS050','I&D PARONCHYIA',30,NULL,NULL),(48,'MS051','NEEDLE ASPIRATION',30,NULL,NULL),(49,'MS052','JADELLE INSERTION',60,NULL,NULL),(50,'MS053','CATHETERISATION',50,NULL,NULL),(51,'MS054','SUPRAPUBIC CATHETERISATION',100,NULL,NULL),(52,'MS055','LUMBAR PUNCTURE',100,NULL,NULL),(53,'MS056','GASTRIC LAVARGE',110,NULL,NULL),(54,'MS057','EAR SYRINGING',20,NULL,NULL),(55,'MS058','POP FULL  MAX 4POP & 2 0RTHOWOOL',80,NULL,NULL),(56,'MS059','SIMPLE BIOPSY',25,NULL,NULL),(57,NULL,'CHEST + RIBS',65,NULL,NULL),(58,NULL,'CHEST + RIBS',65,NULL,NULL),(59,NULL,'CHEST + RIBS',65,NULL,NULL),(60,NULL,'CHEST + RIBS',65,NULL,NULL),(61,NULL,'CHEST + RIBS',65,35,NULL),(62,NULL,'CHEST + RIBS',65,NULL,NULL),(63,NULL,'CHEST + RIBS',65,35,NULL);

/*Table structure for table `schemes` */

DROP TABLE IF EXISTS `schemes`;

CREATE TABLE `schemes` (
  `catergory` varchar(6) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `value` varchar(50) DEFAULT NULL,
  `limit` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `schemes` */

insert  into `schemes`(`catergory`,`name`,`value`,`limit`) values ('medcat','Platinum','Platinum',NULL),('medcat','Gold','Gold',NULL),('medcat','Silver','Sliver',NULL),('medcat','Bronze','Bronze',NULL),('usrtyp','Administrator','Admin',NULL),('usrtyp','User','User',NULL),('gender','Male','M',NULL),('gender','Female','F',NULL),('usrtyp','Supervisor','Supervisor',NULL),('accsts','Pending','Pending',NULL),('accsts','Active','Active',NULL),('accsts','Suspended','Suspended',NULL),('accsts','Closed','Closed',NULL);

/*Table structure for table `sold_products` */

DROP TABLE IF EXISTS `sold_products`;

CREATE TABLE `sold_products` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `billID` bigint(20) DEFAULT NULL,
  `product` bigint(20) DEFAULT NULL,
  `value` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `sold_products` */

/*Table structure for table `transactions` */

DROP TABLE IF EXISTS `transactions`;

CREATE TABLE `transactions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(1) DEFAULT NULL COMMENT 'D= deposit, W= withdrawal, R= reveseal',
  `amount` double DEFAULT NULL,
  `account` bigint(20) DEFAULT NULL,
  `ref` varchar(50) DEFAULT NULL,
  `branch` bigint(20) DEFAULT NULL,
  `method` varchar(45) DEFAULT NULL,
  `insTS` timestamp NULL DEFAULT NULL,
  `insU` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `transactions` */

/*Table structure for table `treatment` */

DROP TABLE IF EXISTS `treatment`;

CREATE TABLE `treatment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoice` bigint(20) DEFAULT NULL,
  `product` bigint(20) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `qty` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `treatment` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
