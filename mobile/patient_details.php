<?php
session_start(); // Use session variable on this page. This function must put on the top of page.

if(!isset($_SESSION['username']) ){ // if session variable "username" does not exist.
header("location:login.php?msg=Please%20login%20to%20access%20admin%20area%20!"); // Re-direct to index.php
}
elseif (isset($_SESSION['username']) && ($_SESSION['usertype'] =='Admin' || $_SESSION['usertype'] =='Supervisor' || $_SESSION['usertype'] =='User' || $_SESSION['usertype'] =='Patient'))
{
	include_once "db.php"; 
	error_reporting (E_ALL ^ E_NOTICE);


?>
<!DOCTYPE HTML>
<html>
<head>
<title>BMC Mobile App</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<style type="text/css">
body,td,th {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #FFFFFF;
}
</style>
</head>
<body>
   <div class="header">	
    <div class="header-top">
       <div class="wrap"> 
	         <div class="logo">
				<a href="index.html"><img src="images/logo.png" alt="" /></a>
			 </div>
			 <div class="cssmenu"> </div>
		    <div class="clear"></div>
	   </div>
	 </div>
	        <div class="header-bottom" id="section-1">
				<div class="wrap"></div>
  			</div>
 		</div>
   <!-- End Main -->
	   <!-- Footer -->
       
         <div class="footer" id="section-5">
    	   <div class="wrap">
              <div class="footer-top">
                <div class="section group">
				<div class="col_1_of_3 span_1_of_3">					
					<h3>WELCOME <?php echo $_SESSION['username']; ?></h3><div id="content">
 <?php
				
			if(isset($_GET['sid']))
				$id=$_GET['sid'];
				
				$line = $db->queryUniqueObject("SELECT * ,DATE_FORMAT(`dob`, '%d/%m/%Y ')dob1 FROM persons p  WHERE p.id=$id");
				if($line->role=='D')
				$line = $db->queryUniqueObject("SELECT * ,DATE_FORMAT(`dob`, '%d/%m/%Y ')dob1 FROM persons p inner join dependant d on p.id= d.dependent WHERE p.id=$id");
			
				
				?>
      <h1> <?php
	  if ($line->role=="P") echo "Patient Details"; elseif($line->role=="D") echo "Dependant Details"; else  echo "Member Details";?></h1>
      
     <form action="" method="post">
	 <table>
	 <tr>
	 <td>
       <table   border="0" cellspacing="0" cellpadding="0">
	   <?php if($line->role!="W"){?>
	    <!--<tr>
           <td width="155">Category:</td>
          <td><?php echo $line->category; ?></td>
         </tr>-->
         <?php }?>
		  <td width="155">Patient Number:
           </td>
           <td width="473"><?php echo $line->pat_number; ?></td>
         </tr>
         <tr>
           <td width="155">Firstname:</td>
           <td width="20"><?php echo $line->firstname; ?></td
         ></tr>         
         <tr>
           <td width="155">Surname:</td>
           <td width="20"><?php echo $line->surname; ?></td>
         </tr>
         <tr>
           <td>Gender: </td>
           <td><?php if($line->gender=='F') echo "Female"; else echo "Male"; ?></td>
         </tr>
         <tr>
           <td width="155">ID Number:</td>
           <td width="473"><?php echo $line->id_number; ?></td>
         </tr> 
         <tr>
           <td>Date of Birth:</td>
           <td><?php echo $line->dob1; ?></td>
         </tr>
		 <?php
		 if($line->role=="P" || $line->role=="M" || $line->role=="W")
		 {
		 ?>
         <tr>
           <td width="155">Address:</td>
           <td width="473"><?php echo $line->address; ?></td>
         </tr> <tr>
           <td width="155">City:</td>
           <td width="473"><?php echo $line->city; ?></td>
         </tr> <tr>
           <td width="155">Phone:</td>
           <td width="473"><?php echo $line->phone; ?></td>
         </tr> <tr>
           <td width="155">Email:</td>
           <td width="473"><?php echo $line->email; ?></td>
         </tr>  <tr>
          
		  <tr>
                      <td> Dependants:</td>
                      <td><a href="dependants.php?c=dependants&id=<?php echo $line->id;?>">View Dependants (<?php   $count = $db->queryUniqueObject("SELECT COUNT(*) num FROM dependant INNER JOIN persons ON persons.id = dependent WHERE holder= ".$line->id." AND persons.active = 1 ");echo $count->num;?>)</a></td>
                    </tr>
         
		 <?php }
		 if($line->role=="D"){
		 $holder = $db->queryUniqueObject("SELECT * FROM persons WHERE id=".$line->holder);
		 ?>
		 <tr>
           <td width="155">Account Owner:
           </td>
           <td width="473"><?php echo $holder->firstname." ".$holder->surname." ( ".$holder->pat_number." )"; ?></td>
         </tr>
		 <tr>
           <td width="155">Relationship:
           </td>
           <td width="473"><?php echo $line->relationship; ?></td>
         </tr>
		 <?php
		 }
		 ?><tr>
          <!--<td align="right"><input type="reset" name="Reset" value="Reset" />
             &nbsp;&nbsp;&nbsp;</td>
           <td>&nbsp;&nbsp;&nbsp;
             <input type="submit" name="Submit" value="Save" /></td>-->
         </tr>
         <tr>
           <td align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
           <td align="left">&nbsp;&nbsp;</td>
         </tr>
       </table>
	   </td><td> <img width="125" height="125" alt="" <?php if($line->image!='') echo "src='upload/profile/$line->pat_number.$line->image'"; else echo "src='images/logo2.gif'"; ?> class="imgr"></td>
	   </tr>
	   </table>
      
     </form>
     <div align="justify"></div>
<div id="respond"></div>
    </div></p>
                  <p>&nbsp;</p>
				</div>
                </div>
            </div> 
         </div>    
          <div class="footer-bottom">
            <div class="copy">
		      <p> © All Rights Reserved 2014 BMC</p>
	       </div>	    
	     </div>   
     </div>
  </body>
</html>
<?php } ?>
    	
    	
            