<?php
session_start(); // Use session variable on this page. This function must put on the top of page.

if(!isset($_SESSION['username']) ){ // if session variable "username" does not exist.
header("location:login.php?msg=Please%20login%20to%20access%20admin%20area%20!"); // Re-direct to index.php
}
elseif (isset($_SESSION['username']) && ($_SESSION['usertype'] =='Admin' || $_SESSION['usertype'] =='Supervisor' || $_SESSION['usertype'] =='User' ))
{
	include_once "db.php"; 
	error_reporting (E_ALL ^ E_NOTICE);


?>
<!DOCTYPE HTML>
<html>
<head>
<title>BMC</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<link href="css/style2.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type='text/javascript' src='js/jquery-1.3.2.min.js'></script>

<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.base.css"/>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.core.css"/>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.theme.css"/>
<link rel="stylesheet" type="text/css" href="libs/jquery/ui.jquery.datePicker.css"/>

<script type="text/javascript" src="libs/jquery/jquery.maskedinput.js"></script>
<script type="text/javascript" src="libs/jquery/jquery.autocomplete.js"></script>
<script type="text/javascript" src="libs/jquery/ui.datePicker.js"></script>
<script type="text/javascript" src="libs/jquery/ui.datepicker.js"></script>
	

<link rel="stylesheet" media="screen" type="text/css" href="css/datepicker.css" />
<script type="text/javascript" src="js/datepicker.js"></script>
<script type='text/javascript' src='js/common.js'></script>
	
<script type='text/javascript' src='js/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />

<link rel='stylesheet' type='text/css' href='css/<?php echo $_GET['c'];?>.css' />
<script type='text/javascript' src='js/<?php echo $_GET['c'];?>.js'></script>
	<link rel='stylesheet' type='text/css' href='css/print.css' media="print" />
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style2.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300' rel='stylesheet' type='text/css'>
<style type="text/css">
body,td,th {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	font-size: 12px;
}
a {
	font-size: 14px;
}
a:link {
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
}
a:active {
	text-decoration: none;
}
</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
					<script type="text/javascript">
					jQuery(document).ready(function($) {
						$(".scroll").click(function(event){		
							event.preventDefault();
							$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
						});
					});
					</script>
<script type="text/javascript" src="js/jquery.jscrollpane.min.js"></script>
		<script type="text/javascript" id="sourcecode">
			$(function()
			{
				$('.scroll-pane').jScrollPane();
			});
		</script>
<meta charset="utf-8">
<script LANGUAGE="JavaScript">
function confirmSubmit()
{
var agree=confirm("Are you sure you wish to Delete this Entry?");
if (agree)
	return true ;
else
	return false ;
}

function confirmDeleteSubmit()
{
var agree=confirm("Are you sure you wish to Delete Seletec Record?");
if (agree)
	
document.deletefiles.submit();
else
	return false ;
}


function checkAll()
{

	var field=document.forms.deletefiles;
for (i = 0; i < field.length; i++)
	field[i].checked = true ;
}

function uncheckAll()
{
	var field=document.forms.deletefiles;
for (i = 0; i < field.length; i++)
	field[i].checked = false ;
}
// -->
</script>
 <script language="JavaScript">
    <!--
      function onLoad() 
      {
        self.moveTo( 0, 0 ); 
        self.resizeTo( screen.availWidth, screen.availHeight ); 
      }
    // -->
    </script>
</head>
<body>
	<!--- Header Starts Here ---><!--- Header Ends Here --->
	<!-- Aboutus Starts Here ---><!-- Aboutus Ends Here --->
	<!-- Interduce Starts Here  ---->
	<div class="interduce">
		<div class="container">
			<div class="row interduce-row">
			  <div class="col-md-6 inter-row-column"><div class="wrap">
              <div class="footer-top">
                <div class="section group">
				<div class="col_1_of_3 span_1_of_3">					
					<h3>Account Billing</h3>
				  <div id="content">
<?php
//var_dump($_POST);//exit;
			$firstname=mysql_real_escape_string($_POST['firstname']);
			$surname=mysql_real_escape_string($_POST['surname']);
			$gender=mysql_real_escape_string($_POST['gender']);
			$id_number=mysql_real_escape_string($_POST['id_number']);
			$dob=mysql_real_escape_string($_POST['dob']);
			$address=mysql_real_escape_string($_POST['address']);
			$city=mysql_real_escape_string($_POST['city']);
			$phone=mysql_real_escape_string($_POST['phone']);
			$email=mysql_real_escape_string($_POST['email']);
			$category=mysql_real_escape_string($_POST['category']);
			$pat_number=mysql_real_escape_string($_POST['pat_number']);
			$corparateId=mysql_real_escape_string($_POST['corparateId']);
			$payment_method=mysql_real_escape_string($_POST["payment_method"]);
			$ismember=mysql_real_escape_string($_POST["new"]);
			$patientId=mysql_real_escape_string($_POST["patientId"]);
			$total_amount=mysql_real_escape_string($_POST["total_amount"]);
			$trans_fee=mysql_real_escape_string($_POST["trans_fee"]);
			$acc_owner=mysql_real_escape_string($_POST["acc_owner"]);

			//$total_amount-=$trans_fee;
			$branch=$_SESSION['branch'];
			$acc_num= $_GET['acc'];
			$line=$db->queryUniqueObject("SELECT * FROM persons INNER JOIN accounts ON persons.id=OWNER WHERE nature='I' AND acc_number =$acc_num");
				
					if(isset($_POST["total_amount"]))
					{
					
				
					if(true)
					{
					if($line->role!="M"){
					if($line->balance >= $total_amount){
						//////////////////inserting bill//////
					$amount=$total_amount-$trans_fee;
						$db->query("INSERT INTO `bill`(`id`,  `patient`,  `date_of_sevice`, `status`, `method_of_payment`,  total_bill, branch,trans_fee,acc_owner)
						VALUES (NULL,' $patientId', NOW(),'Paid', '$payment_method','$amount','$branch','$trans_fee',' $acc_owner')");
		
						$billId=mysql_insert_id();
				
						$db->query("INSERT INTO `transactions`(`id`,`type`,`amount`, `account`, `ref`,insTS,insU,branch)VALUES (NULL,'W','$total_amount','$line->acc_number','$billId',NOW(),'".$_SESSION['username']."','".$_SESSION['branch']."')");
						$new_bal=$line->balance -$total_amount;
						//$new_bal=0;
						if($db->query("UPDATE accounts  SET balance='$new_bal' WHERE acc_number= ".$line->acc_number)){
					
						$branch=$db->queryUniqueObject("SELECT * FROM accounts  WHERE nature='B' AND owner =".$_SESSION['branch']);
						
						$branch_bal=$branch->balance + $amount;
						$db->query("UPDATE accounts  SET balance='$branch_bal' WHERE acc_number= ".$branch->acc_number);
						
						/////////////////////////////////////////////////////
						
						$global=$db->queryUniqueObject("SELECT * FROM accounts  WHERE acc_number= 2");
						
						$global_bal=$global->balance + $trans_fee;
						$db->query("UPDATE accounts  SET balance='$global_bal' WHERE acc_number= 2");
							
					}
					//var_dump($_POST);
		
					for($i=0;$i<count($_POST['qty']);$i++){
					
					if ($_POST['cost'][$i][0]=='$') $cash=substr($_POST['cost'][$i], 1); else $cash=$_POST['cost'][$i];
					$db->query("
					INSERT INTO `treatment` (`id`, `invoice`, `product`,  `price`,  `qty`)
			VALUES (NULL, '$billId', '".$_POST['productid'][$i]."', '".$cash."',   '".$_POST['qty'][$i]."');");
					
			
		}
					
echo "<br><font color=green size=+2 >  Payment was processed successfully!</font>" ;
							}
							else{
								echo "<br><font color=white size=+2 >  Account Has Insufficient Balance!</font>" ;
							}
						}
		else{
			$inline=$db->queryUniqueObject("SELECT  * FROM corporate_members INNER JOIN `accounts` ON `corparateId`=OWNER WHERE  nature ='c' AND memberId =".$line->id);

			if($inline->balance >=$total_amount){
			//////////////////inserting bill//////
					$amount=$total_amount-$trans_fee;
			$db->query("	INSERT INTO `bill`(`id`,  `patient`,  `date_of_sevice`, `status`, `method_of_payment`,  total_bill, branch,trans_fee,acc_owner)
				VALUES (NULL,' $patientId', NOW(),'Paid', '$payment_method','$amount','$branch','$trans_fee',' $acc_owner')");
		
				$billId=mysql_insert_id();
				
				$db->query("INSERT INTO `transactions`(`id`,`type`,`amount`, `account`, `ref`,insTS,insU,branch)VALUES (NULL,'W','$total_amount','$line->acc_number','$billId',NOW(),'".$_SESSION['username']."','".$_SESSION['branch']."')");
					$new_bal=$line->balance-$total_amount;
					if($db->query("UPDATE accounts  SET balance='$new_bal' WHERE acc_number= ".$line->acc_number)){
					
					$new_corp_bal=$inline->balance -$total_amount;
					$db->query("UPDATE accounts  SET balance='$new_corp_bal' WHERE acc_number= ".$inline->acc_number);
					
					$branch=$db->queryUniqueObject("SELECT * FROM accounts  WHERE nature='B' AND owner =".$_SESSION['branch']);
						$branch_bal=$branch->balance + $amount;
						
						$db->query("UPDATE accounts  SET balance='$branch_bal' WHERE acc_number= ".$branch->acc_number);
						
						$global=$db->queryUniqueObject("SELECT * FROM accounts  WHERE acc_number= 2");
						
						$global_bal=$global->balance + $trans_fee;
						$db->query("UPDATE accounts  SET balance='$global_bal' WHERE acc_number= 2");
				
					}
					
					
	
		
					for($i=0;$i<count($_POST['qty']);$i++){
					
					if ($_POST['cost'][$i][0]=='$') $cash=substr($_POST['cost'][$i], 1); else $cash=$_POST['cost'][$i];
					$db->query("
					INSERT INTO `treatment` (`id`, `invoice`, `product`,  `price`,  `qty`)
			VALUES (NULL, '$billId', '".$_POST['productid'][$i]."', '".$cash."',   '".$_POST['qty'][$i]."');");
					
			
		}
echo "<br><font color=green size=+2 >  Payment was processed successfully!</font>" ;
	}
else{
echo "<br><font color=white size=+2 >  Corporate Account Has Insufficient Balance!</font>" ;
}
}			
			}	else{
			echo "<br><font color=white size=+2 >  Account Has Insufficient Balance!</font>" ;
			}	
		}			
						
?>


<form method="post">
<input type="hidden" name ="branch" id="branch" value="<?php echo $_SESSION['branch'];?>" />
<div class="tabs">
    <ul>
        <li><a name="tab" id="tab_1" href="javascript:void(0)" onClick="tabs(1)" class="active">Patient Details</a></li>
        <li><a name="tab" id="tab_2" href="javascript:void(0)" onClick="tabs(2)" class="none">Treatment</a></li>
		 <li><a name="tab" id="tab_3" href="javascript:void(0)" onClick="tabs(3)">Payment</a></li>
    </ul>
</div>
<div name="tab_content" id="tab_content_1" class="tab_content active">
   <table>

	<tr>
           <td width="155">Account Number:</td>
           <td width="473"><span><?php echo $acc_num; ?></span>
		   
		   </td>
         </tr>
	
         <tr>
           <td width="155">Account Holder:</td>
           <td width="473"><span><?php echo $line->firstname ." ".$line->surname; ?></span>
		   <input type="hidden" name="acc_owner" value="<?php echo $line->id;?>"/>
		   </td>
         </tr>         
         <tr>
           <td width="155">Patient:</td>
           
           <td><label for="patientId"></label>
             <select name="patientId" id="patientId">
			 <option value="">Please Select a Patient</option>
               <option value="<?php echo $line->id;?>">Self</option>
			   <?php
			   $result = mysql_query("SELECT * FROM dependant INNER JOIN persons ON dependent=persons.id WHERE holder=".$line->id);
		  	while($row = mysql_fetch_array($result))
			{
			   ?>
               <option value="<?php echo $row['id']; ?>"><?php echo $row['firstname']." ".$row['surname']; ?></option>
			   <?php } ?>
            </select></td>
         </tr>
         <!--<tr>
           <td width="155">ID Number:</td>
           <td width="473"><input name="id_number" type="text" id="id_number"  class="validate[required,length[0,100]] text-input" value="<?php echo $line->id_number; ?>"/></td>
         </tr> 
         <tr>
           <td>Date of Birth:</td>
           <td><input name="dob" type="text" id="dob"  class="validate[optional,custom[onlyNumber],length[6,15]] text-input" value="<?php echo $line->dob; ?>"/></td>
         </tr>
		 
         <tr>
           <td width="155">Address:</td>
           <td width="473"><textarea name="address" id="address" cols="15"><?php echo $line->address; ?></textarea></td>
         </tr> <tr>
           <td width="155">City:</td>
           <td width="473"><input name="city" type="text" id="city"  class="validate[required,length[0,100]] text-input" value="<?php echo $line->city; ?>"/></td>
         </tr> <tr>
           <td width="155">Phone:</td>
           <td width="473"><input name="phone" type="text" id="phone"  class="validate[required,length[0,100]] text-input" value="<?php echo $line->phone; ?>"/></td>
         </tr> <tr>
           <td width="155">Email:</td>
           <td width="473"><input name="email" type="text" id="email"  class="validate[required,length[0,100]] text-input" value="<?php echo $line->email; ?>"/></td>
         </tr>  
		 <tr>
           <td width="155">Patient Number:
            <?php
					  $max = $db->maxOfAll("id","persons");
					  $max=$max+1;
					  $autoid="PAT".str_pad($max, 4, "0", STR_PAD_LEFT)."-00";
					  ?>
					  <input type ="hidden" name="patientId" id="patientId" value="<?php $line->id;?>"/>
					  </td>
           <td width="473"><input name="pat_number" type="text"  id="pat_number" class="validate[required,length[0,100]] text-input pat_number" value="<?php  echo $line->pat_number; //if (isset($_GET['sid'])) echo $line->pat_number; else echo $autoid; ?>"  readonly /></td>
         </tr>-->
	
	
</table>
</div>
<?php 
$branch=$db->queryUniqueObject("SELECT * FROM branch  WHERE id =".$_SESSION['branch']);

?>
<div name="tab_content" id="tab_content_2" class="tab_content">
  
		
	
		
		<table id="items" class="wrap" width="103%">
		
		  <tr>
		      <th width="18%">Item</th>
		      <th width="31%">Description</th>
		      <th width="20%">Unit Cost</th>
		      <th width="20%">Quantity</th>
		      <th width="11%">Price</th>
		  </tr>
		  
		  <tr class="item-row">
		      <td class="item-name"><div class="delete-wpr"><input type="text"  id="product" name="product[]" placeholder="Item Name" />
			  <input type="hidden"  class="productid" id="productid" name="productid[]" <?php if ($branch->type== 'P') echo 'value="0"';?>/>
			  <a class="delete" href="javascript:;" title="Remove row">X</a></div></td>
		      <td class="description"><span class= "description"><?php if ($branch->type== 'C') echo "Description"; else echo "Pharmacy Drug Dispensation ";?></span></td>
		      <td><input type="text" class="cost" name="cost[]" value="$0.00" <?php if ($branch->type== 'C') echo "readonly"; ?>/></td>
		      <td><?php if ($branch->type== 'C'){ ?>
                <input  type="text"  name="qty[]2" class="qty"  value="0"/>
                <?php }
			  else{ ?>
			  <input  type="text"  name="qty[]" class="qty"  value="1"/>
			 <?php } ?></td>
		      <td><span class="price">$0.00</span></td>
		  </tr>
		  
		 
		 <?php 
	if ($branch->type== 'C'){
	?> 
		  <tr id="hiderow">
		  
		    <td colspan="5"><a id="addrow" href="javascript:;" title="Add a row">Add a row</a></td>
	
		  </tr>
		  <?php } ?>
		 <tr id="hiderow">
		   
			<td colspan="5"><a id="cal" href="javascript:;" title="Calculate">Calculate</a></td>
		  </tr>
		 <tr>
		   <td colspan="2" class="blank"> </td>
		   <td colspan="2" class="total-line">Subtotal</td>
		   <td class="total-value"><div id="subtotal">$0.00</div></td>
		   </tr>
		   <tr>

		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Transaction Fee</td>
		      <td class="total-value"><div id="transaction">$0.00</div>
			  <input type="hidden" name="trans_fee" id="trans_fee">
			  </td>
		  </tr>
		  <tr>

		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Total</td>
		      <td class="total-value"><div id="total">$0.00</div></td>
		  </tr>
		 <!-- <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Amount Paid</td>

		      <td class="total-value"><input name="pat_number" type="text"  class="validate[required,length[0,100]] text-input qty" id="paid" value="$0.00" /></td>
		  </tr>-->
		  <input name="paid" type="hidden"  class="validate[required,length[0,100]] text-input qty" id="paid" value="$0.00" />
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line balance">Balance Due</td>
			  <input type="hidden" name="total_amount" id="total_amount"/>
		      <td class="total-value balance"><div class="due">$0.00</div></td>
		  </tr>
		
		</table>
		
		<!--<div id="terms">
		  <h5>Terms</h5>
		  NET 30 Days. Finance Charge of 1.5% will be made on unpaid balances after 30 days.
		</div>-->
	
	</div>
	
	
	
	
	
	<div name="tab_content" id="tab_content_3" class="tab_content">
		<div id="customer">

           
            <table id="meta">
                <tr>
                    <td class="meta-head">Invoice #</td>
                    <td><span><?php
					
					  $max = $db->maxOfAll("id","bill");
					  $max=$max+1;
					  $autoid=str_pad($max, 6, "0", STR_PAD_LEFT);
					  echo $autoid;
					?></span></td>
                </tr>
                <tr>

                    <td class="meta-head">Date</td>
                    <td><span id="date"><?php $today = date("j, n, Y");  echo $today;?></span></td>
                </tr>
                <tr>
                    <td class="meta-head">Amount Due</td>
                    <td><div class="due">$0.00</div></td>
                </tr>
<!-- <tr>
                    <td class="meta-head">Payment Method</td>
                    <td>
						<input type="radio" name="payment_method" id="type" value="cash">Cash<br>
						<input type="radio" name="payment_method" id="type"  value="mobile">Mobile Cash<br>
						<input type="radio" name="payment_method" id="type"  value="zimswitch">Zimswitch<br>
						<input type="radio" name="payment_method" id="type"  value="ewallet">e-Wallet<br>
					</td>
                </tr>-->
            </table>
		
		</div>
		<table><tr>
           <td align="right"><input type="reset" name="Reset" value="Reset" />
             &nbsp;&nbsp;&nbsp;</td>
           <td id= "submit">&nbsp;&nbsp;&nbsp;
		   
		   <a href="#"  class="login-window" id="login-window"><input type="submit" id="Save" value="Save" name="Submit"></a>
             <!--<input type="submit" name="Submit" value="Save" id="Save"/>
			 
			 <input type= "hidden" name="pin" id="pin"/>-->
			 </td>
         </tr></table>
	</div>
	
	<script type='text/javascript'>
 // $(document).ready(function()
  //{
   
    //$('#pinbtn').click(function()
	function pincheck() 
     {
	   var pin = $("#pin").val();
   var id = $("#id").val();
   var amount=$("#total_amount").val();
	 //if ($('[name="payment_method"]:checked').val()== "ewallet") {
         var request = $.ajax(
         {
             url:'pin.php',
             type:'POST',
             data:{pin:pin, id:id,amount:amount},
             success:function(data)
             {  
			  //
if(data=="Withdraw")	{
$('#Save').click(); 
}	
else if(data=="supervisior"){

$('.login-window').click();
//$('#login-window').attr("href","#");
}	
 else {
 
alert(data);
 $('.pin-window').click();
 }	
 
               
             }
         });
  } 
  //});       

//});


function supervisorcheck() 
     {
	   var password = $("#password").val();
   var username = $("#username").val();
   var amount=$("#total_amount").val();
	 //if ($('[name="payment_method"]:checked').val()== "ewallet") {
         var request = $.ajax(
         {
             url:'supervisor.php',
             type:'POST',
             data:{password:password, username:username,amount:amount},
             success:function(data)
             {  
			 // alert(data);
if(data=="Authorize")	{
$('#Save').click(); 
}	
	
 else {
 

  alert(data);
 }	
 
               
             }
         });
  } 
</script>
	<!--pin code
	
	
 <a href="#login-box" class="login-window">Enter Pin</a>-->
  
  <div id="pin-box" class="login-popup">
<!--<a href="#" class="close"><img src="close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>
  <form method="post" class="signin" action="#">-->
        <fieldset class="pin_textbox">
       <!-- <label class="username">
        <span>Username or email</span>
        <input id="username" name="username" value="" type="text" autocomplete="on" placeholder="Username">
        </label>-->
        <label class="password">
        <span>Pin</span>
        <input id="pin" name="pin" value="" type="password" placeholder="Pin">
        </label>
		<a href="#pin-box" class="pin-window"   ></a>
		<a href="#login-box" class="login-window"   ></a>
		<a href="#"   onClick="return pincheck();"><input class="submit pin_button" type="button" name="Submit" value="Submit" id="pinbtn"/></a>
        <!--<button class="submit pin_button" type="button">Submit</button>-->
		<a href="#" class="close"><button class="btn_close pin_button" type="button">Close</button></a>
       <!-- <p>
        <a class="forgot" href="#">Forgot your password?</a>
        </p> -->       
	
        </fieldset>
<!--</form>-->
</div>

 <div id="login-box" class="login-popup">
<!--<a href="#" class="close"><img src="close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>
  <form method="post" class="signin" action="#">-->
        <fieldset class="pin_textbox">
        <label class="username">
        <span>Username </span>
        <input id="username" name="username" value="" type="text" autocomplete="on" placeholder="Username">
        </label>
        <label class="password">
        <span>Password</span>
        <input id="password" name="password" value="" type="password" placeholder="Password">
        </label>
		<input class="submit pin_button" type="button" name="Submit" value="Submit" onClick="return supervisorcheck();" />
        <!--<button class="submit pin_button" type="button">Submit</button>-->
		<a href="#" class="close"><button class="btn_close pin_button" type="button">Close</button></a>
       <!-- <p>
        <a class="forgot" href="#">Forgot your password?</a>
        </p> -->       
	
        </fieldset>
<!--</form>-->
</div>

	</form>
</div>
                  <p>&nbsp;</p>
				</div>
                </div>
            </div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- Interduce Ends Here  ---->
	<!--- Slider Starts Here --->
	
		</div>
	</div>
	<!--- Slider Ends Here --->
	<!--- Portfolio Starts Here ---><!--- Portfolio Ends Here --->
	<!-- Footer Starts Here ---->
	<div class="footer">
		<div class="container">
			<div class="footer-top">
				 <a href="index.html"><img src="images/logo-bot.png" class="img-responsive" alt=""/></a>
			</div>
			<p class="footer-head">BMC <a href="http://w3layouts.com/"></a></p>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- Footer Ends Here ---->
</body>
</html>
	<?php }?>
