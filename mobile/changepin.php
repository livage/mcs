<?php
session_start(); // Use session variable on this page. This function must put on the top of page.

if(!isset($_SESSION['username']) ){ // if session variable "username" does not exist.
header("location:login.php?msg=Please%20login%20to%20access%20admin%20area%20!"); // Re-direct to index.php
}
elseif (isset($_SESSION['username']) && ($_SESSION['usertype'] =='Admin' || $_SESSION['usertype'] =='Supervisor' || $_SESSION['usertype'] =='User' || $_SESSION['usertype'] =='Patient'))
{
	include_once "db.php"; 
	error_reporting (E_ALL ^ E_NOTICE);


?>
<!DOCTYPE HTML>
<html>
<head>
<title>BMC Mobile App</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<style type="text/css">
body,td,th {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #FFFFFF;
}
</style>
</head>
<body>
   <div class="header">	
    <div class="header-top">
       <div class="wrap"> 
	         <div class="logo">
				<a href="index.html"><img src="images/logo.png" alt="" /></a>
			 </div>
			 <div class="cssmenu"> </div>
		    <div class="clear"></div>
	   </div>
	 </div>
	        <div class="header-bottom" id="section-1">
				<div class="wrap"></div>
  			</div>
 		</div>
   <!-- End Main -->
	   <!-- Footer -->
       
         <div class="footer" id="section-5">
    	   <div class="wrap">
              <div class="footer-top">
                <div class="section group">
				<div class="col_1_of_3 span_1_of_3">					
					<h3>Change Your Access PIN # or Password</h3>
                  <p><div id="content">
 <?php
//var_dump($_SESSION);
if(count($_POST)>0) {
if($_SESSION['usertype']=='Patient')
{
$result = mysql_query("SELECT * from _user WHERE personId = " . $_SESSION["patientId"]);
$row=mysql_fetch_array($result);
if($_POST["currentPassword"] == $row["password"]) {
mysql_query("UPDATE _user set password='" . $_POST["newPassword"] . "' WHERE personId = " . $_SESSION["patientId"]);
mysql_query("UPDATE accounts set pin_number='" . $_POST["newPassword"] . "' WHERE owner=" . $_SESSION["patientId"] );
$message = "<br><font color=green size=+1 > PIN # / Password Changed Successfully!</font>";
} else $message = "<br><font color=white size=+1 >Current Password is not correct</font>";
}
else{
$result = mysql_query("SELECT * from _user WHERE username = '" . $_SESSION["username"]."'");
//echo "SELECT * from _user WHERE username = '" . $_SESSION["username"]."'";
$row=mysql_fetch_array($result);
if($_POST["currentPassword"] == $row["password"]) {
mysql_query("UPDATE _user set password='" . $_POST["newPassword"] . "' WHERE username = '" . $_SESSION["username"]."'");
//mysql_query("UPDATE accounts set pin_number='" . $_POST["newPassword"] . "' WHERE owner=" . $_SESSION["patientId"] );
$message = "<br><font color=green size=+1 > PIN / Password Changed Successfuly!</font>";
} else $message = "<br><font color=white size=+1 >Current Password is not correct</font>";
}
}
?>
 
 <script>
function validatePassword() {
var currentPassword,newPassword,confirmPassword,output = true;

currentPassword = document.frmChange.currentPassword;
newPassword = document.frmChange.newPassword;
confirmPassword = document.frmChange.confirmPassword;

if(!currentPassword.value) {
	currentPassword.focus();
	document.getElementById("currentPassword").innerHTML = "  required field";
	output = false;
}
else if(!newPassword.value) {
	newPassword.focus();
	document.getElementById("newPassword").innerHTML = "  required field";
	output = false;
}
else if(!confirmPassword.value) {
	confirmPassword.focus();
	document.getElementById("confirmPassword").innerHTML = "  required field";
	output = false;
}
if(newPassword.value != confirmPassword.value) {
	newPassword.value="";
	confirmPassword.value="";
	newPassword.focus();
	document.getElementById("confirmPassword").innerHTML = "  password unmatched";
	output = false;
} 	
return output;
}
</script>
 
<form name="frmChange" method="post" action="" onSubmit="return validatePassword()">
<div style="width:500px;">
<div class="message"><?php if(isset($message)) { echo $message; } ?></div>
<table border="0" cellpadding="10" cellspacing="0" width="500" align="center" class="tblSaveForm">

<tr>
<td width="40%"><label>Current PIN / Password</label></td>
<td width="60%"><input type="password" name="currentPassword" class="txtField"/><span id="currentPassword"  class="required"></span></td>
</tr>
<tr>
<td><label>New PIN / Password</label></td>
<td><input type="password" name="newPassword" class="txtField"/><span id="newPassword" class="required"></span></td>
</tr>
<td><label>Confirm PIN / Password</label></td>
<td><input type="password" name="confirmPassword" class="txtField"/><span id="confirmPassword" class="required"></span></td>
</tr>
<tr>
<td colspan="2"><input type="submit" name="submit" value="Submit" class="btnSubmit"></td>
</tr>
</table>
</div>
</form>
 </div>
       <p>&nbsp;</p>
       <p><a href="logout.php"><img src="images/logout.gif"></a></p>
	 
      
    </html>
     
<?php } ?>
    	
    	
            