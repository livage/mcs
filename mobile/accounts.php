<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>BMC Mobile App</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
   <div class="header">	
    <div class="header-top">
       <div class="wrap"> 
	         <div class="logo">
				<a href="index.html"><img src="images/logo.png" alt="" /></a>
			 </div>
			 <div class="cssmenu"> </div>
		    <div class="clear"></div>
	   </div>
	 </div>
	        <div class="header-bottom" id="section-1">
				<div class="wrap"></div>
  			</div>
 		</div>
   <!-- End Main -->
	   <!-- Footer -->
         <div class="footer" id="section-5">
    	   <div class="wrap">
              <div class="footer-top">
                <div class="section group">
				<div class="col_1_of_3 span_1_of_3">					
					<h3>CHOOSE ACCOUNT TYPE TO VIEW</h3>
				  <p><a href="view_individual_account.php"><img src="images/individual_acc.gif"></a></p>
				  <p><a href="view_corporate_account.php"><img src="images/corporate_acc.gif"></a></p>
				  <p>&nbsp;</p>
				  <p><a href="logout.php"><img src="images/logout.gif"></a></p>
                  <p>&nbsp;</p>
				</div>
				<div class="col_1_of_3 span_1_of_3"></div>
				 <div class="col_1_of_3 span_1_of_3">
					<h3></h3>
					<p><span> </span></p>
				</div>
			  </div>
            </div> 
         </div>    
          <div class="footer-bottom">
            <div class="copy">
		      <p> © All Rights Reserved 2014 BMC</p>
	       </div>	    
	     </div>   
     </div>
  </body>
</html>

    	
    	
            