<?php
$id=$_GET['id'];
			$line=$db->queryUniqueObject("SELECT * FROM bill INNER JOIN persons ON persons.id=acc_owner WHERE bill.id=$id");
				//var_dump($line);
				
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	
	<title>Invoice</title>
	
	<link rel='stylesheet' type='text/css' href='Invoice/css/style.css' />
	<link rel='stylesheet' type='text/css' href='Invoice/css/print.css' media="print" />
	<script type='text/javascript' src='Invoice/js/jquery-1.3.2.min.js'></script>
	<script type='text/javascript' src='Invoice1/js/example.js'></script>

</head>

<body>

	<div id="page-wrap">

		<textarea id="header">INVOICE</textarea>
		
		<div id="identity">
		
            <textarea id="address"><?php echo $line->firstname." ".$line->surname; ?>
<?php echo $line->address; ?>
Phone: <?php echo $line->phone; ?></textarea>

            <div id="logo">

              <div id="logoctr">
                <a href="javascript:;" id="change-logo" title="Change logo">Change Logo</a>
                <a href="javascript:;" id="save-logo" title="Save changes">Save</a>
                |
                <a href="javascript:;" id="delete-logo" title="Delete logo">Delete Logo</a>
                <a href="javascript:;" id="cancel-logo" title="Cancel changes">Cancel</a>
              </div>

              <div id="logohelp">
                <input id="imageloc" type="text" size="50" value="" /><br />
                (max width: 540px, max height: 100px)
              </div>
              <img id="image" src="images/logo.png" alt="logo" />
            </div>
		
		</div>
		
		<div style="clear:both"></div>
		
		<div id="customer">

            <textarea id="customer-title">BMC</textarea>

            <table id="meta">
                <tr>
                    <td class="meta-head">Invoice #</td>
                    <td><textarea readonly><?php echo $line->id; ?></textarea></td>
                </tr>
                <tr>

                    <td class="meta-head">Date</td>
                    <td><textarea id="dateq" readonly><?php echo $line->date_of_sevice; ?></textarea></td>
                </tr>
                <tr>
                    <td class="meta-head">Amount Due</td>
                    <td><div class="due">$<?php echo number_format($line->total_bill+$line->trans_fee,2); ?></div></td>
                </tr>

            </table>
		
		</div>
		
		<table id="items">
		
		  <tr>
		      <th>Item</th>
		      <th>Description</th>
		      <th>Unit Cost</th>
		      <th>Quantity</th>
		      <th>Price</th>
		  </tr>
		  <?php 
		  $result = mysql_query("SELECT * FROM treatment INNER JOIN products_services ON products_services.id=product WHERE invoice=$id");
		  	while($row = mysql_fetch_array($result))
			{
		  ?>
		  <tr class="item-row">
		      <td class="item-name"><div class="delete-wpr"><textarea readonly><?php echo $row['code']; ?></textarea></div></td>
		      <td class="description"><textarea readonly><?php echo $row['description']; ?></textarea></td>
		      <td><textarea class="cost">$<?php echo number_format($row['price'],2); ?></textarea></td>
		      <td><textarea class="qty"><?php echo $row['qty']; ?></textarea></td>
		      <td><span class="price">$<?php  $cost=$row['price']*$row['qty'];echo number_format($cost,2);?></span></td>
		  </tr>
		  <?php } ?>
		 
		  
		  <tr id="hiderow">
		    <td colspan="5"></td>
		  </tr>
		  
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Subtotal</td>
		      <td class="total-value"><div id="subtotal">$<?php echo number_format( $line->total_bill,2); ?></div></td>
		  </tr>
		  <tr>

		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Transactional Fee</td>
		      <td class="total-value"><div id="total">$<?php echo number_format($line->trans_fee,2); ?></div></td>
		  </tr>
		 
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line balance">Balance Due</td>
		      <td class="total-value balance"><div class="due">$<?php echo number_format($line->total_bill+$line->trans_fee,2); ?></div></td>
		  </tr>
		
		</table>
		
		<div id="terms">
		  <h5>Terms</h5>
		  <textarea>NET 30 Days. Finance Charge of 1.5% will be made on unpaid balances after 30 days.</textarea>
		</div>
	
	</div>
	
</body>

</html>