/**
 * uploadFile is called when the event change on the file upload input 
 * element is triggered
 * it submits the form and the rest is handled in ABAP  
 */
function uploadFile(){
    //alert('Hello');
    $("#mode").attr("name", "onInputProcessing(UPLOAD)");
    $("#upload").submit();
    //alert('good bye');
}
/**
 *  deleteAttachment is triggered when the icon delete is called.
 *  it submits and the rest is handled in ABAP
 *    
 */       
function deleteAttachment(index){
    $("#mode").attr("name", "onInputProcessing(DELETE)");
    $("#delete_no").val(index);
    $("#upload").submit();
}

/**
 *  This is run when the page loads
 *   
 */ 
$(document).ready(
  function(){
  
    $('form h2').after('<p class="required">Fields that are marked with <span class="required">*</span> are required.</p>');
    
    var bupa = $("#BP_FOUND").val();

    if(bupa == "FALSE"){
      /**
       *  Business Partner not found,
       *  Redirect to BUPA create form           
       */
      //check if myurl exists then assign current
      //url
      var url = document.referrer;
         
      if ($('#myurl').length > 0) { 
        // it exists
        $('#myurl').attr({
          value: url 
        }); 
      } else{
        //Does not exist
        $("<input />").attr({
          type:"hidden",
          value: url,
          name: "myurl",
          id: "myurl"
        }).appendTo("#upload");
      }
      //create if not exists mode
      
      if ($('#mode').length > 0) {
        $("#mode").attr("name","onInputProcessing(REDIR)");
      }else{
        $("<input />").attr({
          type:"hidden",
          name: "onInputProcessing(REDIR)",
          id: "mode"
        }).appendTo("#upload");
      } 
      if (confirm("You do not have a Business Partner associated with your account.\n" + 
            "\nWould you like to create one now?\n\n" + 
            "(Clicking OK will take you to the Business Partner creation form return you here after you submit.)")) {
      // redirect
        $("#upload").submit();  
      } else {
      /**
       * if we are not going anywhere 
       * might as well load provinces       
       */             
        getProvinces();        
      }
    }else{
      /**
       *  We are not going anywhere so lets load provinces
       */             
      getProvinces();
  }
});
/**
 * This function calls getprovinces.htm in global includes
 * and retrieves a json object which has a list of provinces 
 */ 
function getProvinces(){
  /**
   * The task masters want pretty things, 
   * so instead of overt submit we go the AJAX route 
   * it loads without refreshing the page,
   * 
   *   *_* --> pretty,          
   */     
  $.ajax({
        url:'../ZGVT_INCLUDES/getprovinces.htm',
        dataType:'json',
        type: 'post',
        
        success: function(data){
          $('#PROVINCE option').remove();
          /**
           *  our JSON looks like
           *  {
           *    success: TRUE,
           *    provinces:["one", "two"..."n"],
           *    province: "province",
           *    districts: ["one", "two" ... "n"]                                            
           *  }
           *  
           *  I thought this up when i was still learning, the newer versions
           *  use
           *             
           *  {
           *    success: TRUE,
           *    object: "",           
           *    objects:["one", "two"..."n"]                      
           *  }                                                       
           */                     
          if(data.success){
            var l = data.provinces.length;
            var i;
            
            $('#PROVINCE').append($('<option></option>').attr("value", "Select").text("-- Select Province --"));
            $('#DISTRICT option').remove();
            $('#DISTRICT').append($('<option></option>').attr("value", "Select").text("Please select a province first"));
            
            for(i=0; i<l; i++){
                if (data.provinces[i] == $('#PROVVAL').val()){
                   $('#PROVINCE').append($('<option selected></option>').attr("value", data.provinces[i]).text(data.provinces[i]));
                   $("#PROVINCE").change();  
                }else{
                  $('#PROVINCE').append($('<option></option>').attr("value", data.provinces[i]).text(data.provinces[i])); 
                }
            }
          }else{
            
          }
        },
        error: function (xhr, ajaxOptions, thrownError) {
          alert("Error: " + xhr.status + "\n" + thrownError + "\n" + xhr.responseText);
        }
    });     
    return false;
}



$('#PROVINCE').on('change', function(){
  /**
   * Read about JSON structure above, too lazy to be bothered to copy paste.
   */        

  var name = "PROVINCE";
  var value = $('#PROVINCE').val();
  var dataObj = {};
  
  $('#PROVVAL').attr("value", value);

  //$('#DISTRICT').attr("value", $('#DISTVAL').attr("value", ''));                     
  

  dataObj[name]=value;
  
  $.ajax({
    url: '../zgvt_includes/getdistricts.htm',
    dataType:'json',
    type:'post',
    data:dataObj,
    
    success: function(data){
      if(data.success){
        var l = data.districts.length;
        var i;
      
        $('#DISTRICT option').remove();
        $('#DISTRICT').append($('<option></option>').attr("value", "Select").text("-- Select a District --"));
        
        for(i=0; i<l; i++){
          if (data.districts[i] == $('#DISTVAL').val()){
                  $('#DISTRICT').append($('<option selected></option>').attr("value", data.districts[i]).text(data.districts[i]));
                  $("#DISTRICT").change();
                }else{
                  $('#DISTRICT').append($('<option></option>').attr("value", data.districts[i]).text(data.districts[i]));
                }
           
        }
      }else{
        $('#DISTRICT option').remove();
        $('#DISTRICT').append($('<option></option>').attr("value", "Select").text("Please select a province first"));
      }
    },
    
    error: function(xhr, ajaxOptions, thrownError){
      alert("Error: " + xhr.status + "\n" + thrownError + "\n" + xhr.responseText);
    }
  });
});

$('#DISTRICT').on('change', function(){
  /**
   * Read about JSON structure above, too lazy to be bothered to copy paste.
   */        
  $('#DISTVAL').attr("value", $('#DISTRICT').val());  
});      

$('#upload').on('submit', function(e){
        //minimum amount of files needed
        var min = $('#min_attachments').val(); 
        if($('#num_attachments').val() < min && $("#mode").attr("name") != "onInputProcessing(UPLOAD)"){
          alert('You are required to attach at least ' + min + ' documents.');
          $('#upload_tbl').focus();
          e.preventDefault();
        }
      });
      
function getTooltips(app){
  var dataObj = {};

          var name = "APP";
          var value = app;
          dataObj[name]=value;

          $.ajax({
            url:'../ZGVT_INCLUDES/getTooltip.htm',
            dataType:'json',
            type: 'post',
            data:dataObj,

            success: function(data){
              if(data.success){
                var l = data.tooltips.length;
                var i;
                 
                for(i=0; i<l; i++){
                  var s = '#' + data.tooltips[i].comp;
                  $(s).tooltipster({
                    position: 'right',
                    contentAsHTML: true,
                    content: data.tooltips[i].tooltip,
                    theme: 'tooltipster-shadow'
                  });
                }
              }
            },
            error: function (xhr, ajaxOptions, thrownError) {
              alert("Error: " + xhr.status + "\n" + thrownError + "\n" + xhr.responseText);
            }         
          });
}