-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 29, 2014 at 12:49 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bmc2`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
  `acc_number` bigint(10) unsigned zerofill NOT NULL,
  `category` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `balance` double DEFAULT NULL,
  `pin_number` varchar(4) DEFAULT NULL,
  `owner` bigint(20) DEFAULT NULL,
  `nature` varchar(1) DEFAULT NULL COMMENT 'C= corparate, I=individual',
  `overdraft` double DEFAULT NULL,
  `insTs` timestamp NULL DEFAULT NULL,
  `insUs` varchar(255) DEFAULT NULL,
  `updTs` timestamp NULL DEFAULT NULL,
  `updUs` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`acc_number`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`acc_number`, `category`, `status`, `balance`, `pin_number`, `owner`, `nature`, `overdraft`, `insTs`, `insUs`, `updTs`, `updUs`) VALUES
(0000000001, 'Loan', 'Active', NULL, '0000', 1, 'G', NULL, NULL, NULL, NULL, NULL),
(0000000002, 'Bmc', 'Active', NULL, '0000', 2, 'G', NULL, NULL, NULL, NULL, NULL),
(1234567890, 'Platinum', 'Active', 10, '0000', 1, 'I', 0, '2014-08-29 10:22:24', 'root', '2014-08-29 10:22:41', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

CREATE TABLE IF NOT EXISTS `bill` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `patient` bigint(20) DEFAULT NULL,
  `date_of_sevice` datetime DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL COMMENT 'paid, outstanding',
  `method_of_payment` varchar(50) DEFAULT NULL COMMENT 'cash, account, zimswitch',
  `total_bill` double DEFAULT NULL,
  `branch` bigint(20) DEFAULT NULL,
  `acc_owner` bigint(20) DEFAULT NULL,
  `trans_fee` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `bill`
--

INSERT INTO `bill` (`id`, `patient`, `date_of_sevice`, `status`, `method_of_payment`, `total_bill`, `branch`, `acc_owner`, `trans_fee`) VALUES
(1, 1, '2014-08-29 12:25:29', 'Paid', '', 110, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(8) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `active` binary(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `code`, `name`, `address`, `active`) VALUES
(1, 'BMC0001', 'Loan Account', '', '1'),
(2, 'BMC0002', 'BMC ', '', '1'),
(3, 'BMC0003', 'BMC Pharmacy', '189 Samora Machel Avenue', '1'),
(4, 'BMC0004', 'Baines Clinic', '10 Baines Avenue', '1'),
(5, 'BMC0005', 'West End Clinic', '12 Kingsway Road, Harare', '1');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `catergory` varchar(6) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `value` varchar(50) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`catergory`, `name`, `value`) VALUES
('medcat', 'Platinum', 'Platinum'),
('medcat', 'Gold', 'Gold'),
('medcat', 'Silver', 'Sliver'),
('medcat', 'Bronze', 'Bronze'),
('usrtyp', 'Administrator', 'Admin'),
('usrtyp', 'User', 'User'),
('gender', 'Male', 'M'),
('gender', 'Female', 'F'),
('usrtyp', 'Supervisor', 'Supervisor'),
('accsts', 'Pending', 'Pending'),
('accsts', 'Active', 'Active'),
('accsts', 'Suspended', 'Suspended'),
('accsts', 'Closed', 'Closed');

-- --------------------------------------------------------

--
-- Table structure for table `corporate`
--

CREATE TABLE IF NOT EXISTS `corporate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `reg_num` varchar(255) DEFAULT NULL,
  `inception_date` date DEFAULT NULL,
  `c_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `phone` varchar(56) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `cor_number` varchar(255) DEFAULT NULL,
  `insUs` varchar(255) DEFAULT NULL,
  `insTs` datetime DEFAULT NULL,
  `updUs` varchar(255) DEFAULT NULL,
  `updTs` date DEFAULT NULL,
  `active` binary(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `corporate_members`
--

CREATE TABLE IF NOT EXISTS `corporate_members` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `corparateId` bigint(20) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `joinDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insU` varchar(255) DEFAULT NULL,
  `insTS` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updU` varchar(255) DEFAULT NULL,
  `updTS` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` binary(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dependant`
--

CREATE TABLE IF NOT EXISTS `dependant` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `holder` bigint(20) DEFAULT NULL,
  `dependent` bigint(20) DEFAULT NULL,
  `relationship` varchar(50) DEFAULT NULL,
  `active` binary(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `persons`
--

CREATE TABLE IF NOT EXISTS `persons` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `id_number` varchar(17) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `phone` varchar(56) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pat_number` varchar(255) DEFAULT NULL,
  `role` varchar(1) DEFAULT NULL COMMENT 'P=patient, M=memember, D= dependent,C=coprate,W= walkin',
  `image` varchar(4) DEFAULT NULL COMMENT 'file extsion (jepg,jpg, gif)',
  `insUs` varchar(255) DEFAULT NULL,
  `insTs` datetime DEFAULT NULL,
  `updUs` varchar(255) DEFAULT NULL,
  `updTs` date DEFAULT NULL,
  `active` binary(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `persons`
--

INSERT INTO `persons` (`id`, `firstname`, `surname`, `gender`, `id_number`, `dob`, `address`, `city`, `phone`, `email`, `pat_number`, `role`, `image`, `insUs`, `insTs`, `updUs`, `updTs`, `active`) VALUES
(1, 'Artwell', 'Macheka', 'M', '47gfvkloip', '1947-08-13', 'dgtyb', 'Harare', '0772319013', 'amacheka@gmail.com', 'PAT10001-00', 'P', '0', ' admin', '2014-08-29 12:22:24', NULL, NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `products_services`
--

CREATE TABLE IF NOT EXISTS `products_services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(6) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `category` bigint(20) DEFAULT NULL,
  `branch` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `products_services`
--

INSERT INTO `products_services` (`id`, `code`, `description`, `price`, `category`, `branch`) VALUES
(1, 'MS004', 'JADELLE', 60, NULL, NULL),
(2, 'MS005', 'SPLINTING', 10, NULL, NULL),
(3, 'MS006', 'URINALYSIS', 10, NULL, NULL),
(4, 'MS007', 'RDT MALARIA TEST', 10, NULL, NULL),
(5, 'MS008', 'SUTURE REMOVAL', 5, NULL, NULL),
(6, 'MS009', 'ADMIISSION  RESUS INCLUDING 1 VACOLITRE', 110, NULL, NULL),
(7, 'MS010', 'BP CHECK', 1, NULL, NULL),
(8, 'MS011', 'DEBRIDEMENT UNDER LOCAL', 50, NULL, NULL),
(9, 'MS012', 'COMPLICATED DEBRIDEMENT', 70, NULL, NULL),
(10, 'MS013', 'ECG', 50, NULL, NULL),
(11, 'MS014', 'BACKSLAB MAX 4POP & 2ORTHOWOOL', 70, NULL, NULL),
(12, 'MS015', 'REMOVAL OF SUBCUT FB', 50, NULL, NULL),
(13, 'MS016', 'ENEMA', 120, NULL, NULL),
(14, 'MS017', 'IM + IV INJECTION', 25, NULL, NULL),
(15, 'MS018', 'EYE IRRIGATION', 50, NULL, NULL),
(16, 'MS019', 'INJECTION OUTPATIENT ', 15, NULL, NULL),
(17, 'MS020', 'GLUCOMETER', 10, NULL, NULL),
(18, 'MS021', 'NGT', 5, NULL, NULL),
(19, 'MS022', 'NASAL PACKING', 120, NULL, NULL),
(20, 'MS023', 'N/S SCREEN', 10, NULL, NULL),
(21, 'MS024', 'YELLOW FEVER', 70, NULL, NULL),
(22, 'MS025', 'ANTIRABIES', 50, NULL, NULL),
(23, 'MS026', 'CERVICAL COLLAR', 25, NULL, NULL),
(24, 'MS027', 'PAP SMEAR', 60, NULL, NULL),
(25, 'MS028', 'FOREIGN BODY EASILY ACCESSIBLE', 30, NULL, NULL),
(26, 'MS029', 'ASCITIC TAPING', 100, NULL, NULL),
(27, 'MS030', 'MEDICAL EXAM ONLY', 55, NULL, NULL),
(28, 'MS031', 'MEDICAL EXAM + X-RAY IMMIGRATION', 130, NULL, NULL),
(29, 'MS032', 'X-RAY ONLY IMMIGRATION', 100, NULL, NULL),
(30, 'MS033', 'ECG DOTS (EACH)', 1, NULL, NULL),
(31, 'MS034', 'IV DRESSING /TEGADERM', 2, NULL, NULL),
(32, 'MS035', 'PRIMAPORE DRESSING', 3, NULL, NULL),
(33, 'MS036', 'LEUKOMED T PLUS', 1, NULL, NULL),
(34, 'MS037', 'OPSITE FLEXIGRID', 1, NULL, NULL),
(35, 'MS038', 'LEUKOSTRIP', 3, NULL, NULL),
(36, 'MS039', 'Consultation ', 30, NULL, NULL),
(37, 'MS040', 'Admission  OBS', 55, NULL, NULL),
(38, 'MS041', 'NEBULISATION PER CYCLE  × 1', 15, NULL, NULL),
(39, 'MS042', 'BLOOD COLLECTION', 10, NULL, NULL),
(40, 'MS043', 'PREG TEST', 5, NULL, NULL),
(41, 'MS044', 'IV & IM OWN MEDICINE', 5, NULL, NULL),
(42, 'MS045', 'SIMPLE DRESSING', 12, NULL, NULL),
(43, 'MS046', 'LARGE DRESSING', 15, NULL, NULL),
(44, 'MS047', 'SUTURE MINOR           INCL ATT', 50, NULL, NULL),
(45, 'MS048', 'SUTURE MAJOR/MULTIPLE WOUNDS', 130, NULL, NULL),
(46, 'MS049', 'I & D', 40, NULL, NULL),
(47, 'MS050', 'I&D PARONCHYIA', 30, NULL, NULL),
(48, 'MS051', 'NEEDLE ASPIRATION', 30, NULL, NULL),
(49, 'MS052', 'JADELLE INSERTION', 60, NULL, NULL),
(50, 'MS053', 'CATHETERISATION', 50, NULL, NULL),
(51, 'MS054', 'SUPRAPUBIC CATHETERISATION', 100, NULL, NULL),
(52, 'MS055', 'LUMBAR PUNCTURE', 100, NULL, NULL),
(53, 'MS056', 'GASTRIC LAVARGE', 110, NULL, NULL),
(54, 'MS057', 'EAR SYRINGING', 20, NULL, NULL),
(55, 'MS058', 'POP FULL  MAX 4POP & 2 0RTHOWOOL', 80, NULL, NULL),
(56, 'MS059', 'SIMPLE BIOPSY', 25, NULL, NULL),
(57, NULL, 'CHEST + RIBS', 65, NULL, NULL),
(58, NULL, 'CHEST + RIBS', 65, NULL, NULL),
(59, NULL, 'CHEST + RIBS', 65, NULL, NULL),
(60, NULL, 'CHEST + RIBS', 65, NULL, NULL),
(61, NULL, 'CHEST + RIBS', 65, 35, NULL),
(62, NULL, 'CHEST + RIBS', 65, NULL, NULL),
(63, NULL, 'CHEST + RIBS', 65, 35, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `schemes`
--

CREATE TABLE IF NOT EXISTS `schemes` (
  `catergory` varchar(6) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `value` varchar(50) DEFAULT NULL,
  `limit` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schemes`
--

INSERT INTO `schemes` (`catergory`, `name`, `value`, `limit`) VALUES
('medcat', 'Platinum', 'Platinum', NULL),
('medcat', 'Gold', 'Gold', NULL),
('medcat', 'Silver', 'Sliver', NULL),
('medcat', 'Bronze', 'Bronze', NULL),
('usrtyp', 'Administrator', 'Admin', NULL),
('usrtyp', 'User', 'User', NULL),
('gender', 'Male', 'M', NULL),
('gender', 'Female', 'F', NULL),
('usrtyp', 'Supervisor', 'Supervisor', NULL),
('accsts', 'Pending', 'Pending', NULL),
('accsts', 'Active', 'Active', NULL),
('accsts', 'Suspended', 'Suspended', NULL),
('accsts', 'Closed', 'Closed', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sold_products`
--

CREATE TABLE IF NOT EXISTS `sold_products` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `billID` bigint(20) DEFAULT NULL,
  `product` bigint(20) DEFAULT NULL,
  `value` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(1) DEFAULT NULL COMMENT 'D= deposit, W= withdrawal, R= reveseal',
  `amount` double DEFAULT NULL,
  `account` bigint(20) DEFAULT NULL,
  `ref` varchar(50) DEFAULT NULL,
  `branch` bigint(20) DEFAULT NULL,
  `method` varchar(45) DEFAULT NULL,
  `insTS` timestamp NULL DEFAULT NULL,
  `insU` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `type`, `amount`, `account`, `ref`, `branch`, `method`, `insTS`, `insU`) VALUES
(1, 'D', 120, 1234567890, 'adf', 1, 'cash', '2014-08-29 10:24:36', 'admin'),
(2, 'W', 110, 1234567890, '1', 0, NULL, '2014-08-29 10:25:29', 'amacheka');

-- --------------------------------------------------------

--
-- Table structure for table `treatment`
--

CREATE TABLE IF NOT EXISTS `treatment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoice` bigint(20) DEFAULT NULL,
  `product` bigint(20) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `qty` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_user`
--

CREATE TABLE IF NOT EXISTS `_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `username` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `branch` bigint(20) DEFAULT NULL,
  `active` binary(1) DEFAULT '1',
  `personId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `_user`
--

INSERT INTO `_user` (`id`, `firstname`, `surname`, `username`, `password`, `user_type`, `branch`, `active`, `personId`) VALUES
(1, 'Admin', 'Admin', 'admin', 'admin', 'Admin', 1, '1', NULL),
(2, 'Artwell', 'Macheka', 'PAT10001-00', 'password', 'Patient', 0, '1', 1),
(3, 'Artwell', 'Macheka', 'amacheka', 'exodus', 'User', 0, '1', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
