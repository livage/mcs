<?php
session_start(); // Use session variable on this page. This function must put on the top of page.

if(!isset($_SESSION['username']) ){ // if session variable "username" does not exist.
header("location:login.php?msg=Please%20login%20to%20access%20admin%20area%20!"); // Re-direct to index.php
}
elseif (isset($_SESSION['username']) && ($_SESSION['usertype'] =='Admin' || $_SESSION['usertype'] =='Supervisor' || $_SESSION['usertype'] =='User' || $_SESSION['usertype'] =='Patient'))
{
	include_once "db.php"; 
	error_reporting (E_ALL ^ E_NOTICE);


?>
<!DOCTYPE HTML>
<html>
<head>
<title>BMC Mobile App</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<style type="text/css">
body,td,th {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #FFFFFF;
}
</style>
</head>
<body>
   <div class="header">	
    <div class="header-top">
       <div class="wrap"> 
	         <div class="logo">
				<a href="index.html"><img src="images/logo.png" alt="" /></a>
			 </div>
			 <div class="cssmenu"> </div>
		    <div class="clear"></div>
	   </div>
	 </div>
	        <div class="header-bottom" id="section-1">
				<div class="wrap"></div>
  			</div>
 		</div>
   <!-- End Main -->
	   <!-- Footer -->
       
         <div class="footer" id="section-5">
    	   <div id="content">
 <?php
  
 if ($_SESSION['usertype']=='Patient')
 $id=$_SESSION['username'];
 else
			$id=$_POST['accountNo'];	
$count = $db->countOf("accounts", "acc_number='$id' AND nature ='I'");

if($count>0)
{
$sql="SELECT * FROM persons p INNER JOIN accounts ON p.id=accounts.owner  WHERE nature='I' ";

				
				$line = $db->queryUniqueObject($sql." AND acc_number='$id'");
				if($line->role =="M")
				{
				$sql="SELECT * ,persons.id as pid FROM persons INNER JOIN accounts ON persons.id=OWNER AND nature= 'I' INNER JOIN corporate_members ON memberId=persons.id INNER JOIN corporate ON corparateId=corporate.id WHERE  acc_number='$id'";
				
				//echo $sql;
				$line = $db->queryUniqueObject($sql);
				}
				
			}
			else{
			echo "<script type='text/javascript'>alert('Account does not exist');
			window.location = 'index.php?c=card&app=pi';</script>";
			}
				?>
      <h1>  Patient Informatics</h1>
      
     <form action="" method="post">
	 
	<input type="hidden" name="patientId" value="<?php echo $_POST["patientId"];?>"/>
	
       <table   border="0" cellspacing="0" cellpadding="0">
	   <?php
	  if ($line->nature!="C") { ?>
	    <tr>
           <td width="155">Category:</td>
          <td><?php echo $line->category; ?></td>
         </tr>
		 <?php } ?>
         <tr>
		  <td width="155"><?php if ($line->nature=="C") echo "Corporate"; else  echo "Patient"; ?> Number:
           </td>
           <td width="473"><?php if ($line->nature=="C") echo  $line->cor_number; else echo $line->pat_number ; ?></td>
         </tr>
		 <tr>
		  <td width="155">Account Number:
           </td>
           <td width="473"><?php echo $line->acc_number; ?></td>
         </tr>
		  <tr>
		  <td width="155">Status:
           </td>
           <td width="473"><?php echo $line->status; ?></td>
         </tr>
         <tr>
           <td width="155">Contact person:</td>
           <td width="20"><?php if ($line->nature=="C") echo  $line->c_name; else echo $line->firstname ." ".$line->surname; ?></td>
         </tr> 
<?php if ($line->nature=="C" || $line->name!= ""){?>		 
         <tr>
           <td width="155">Corporate Name:</td>
           <td width="20"><?php echo $line->name; ?></td>
         </tr>
		 <?php }?>
      <tr>
           <td width="155">Patient:</td>
           <td width="473"><?php 
		   $sqll="SELECT * FROM persons  WHERE  persons.id= ";
		   if(isset($_POST["patientId"]) )$sqll.= $_POST["patientId"]; else $sqll.=  $_SESSION['patientId'];
		   $patie=$db->queryUniqueObject( $sqll);
		   echo $patie->firstname." ".$patie->surname; ?>
		   <input type="hidden" name="patient" value="<?php echo $_GET['pid']; ?> " />
		   </td>
         </tr>
        
       </table>
	   <?php
  
	 $sql= " SELECT  * FROM patient_info WHERE patient =  ";
 if(isset($_POST["patientId"]) )$sql.= $_POST["patientId"]; else $sql.=  $_SESSION['patientId'];

$query = "SELECT COUNT(*) as num  FROM `patient_info` WHERE patient =";
if(isset($_POST["patientId"]) )$query.= $_POST["patientId"]; else $query.=  $_SESSION['patientId'];

$query .= $sqlfilter;

	$total_pages = mysql_fetch_array(mysql_query($query));

	$total_pages = $total_pages[num];

	

	/* Setup vars for query. */

	$targetpage = "index.php?c=pi_admin&acc=".$_GET['acc']; 	//your file name  (the name of this file)
	
	
	$limit = 10; 								//how many items to show per page
	 include ('pagination.php');

	/* Get data. */



	
	$sql .= $sqlfilter;

	$result = mysql_query($sql." ORDER BY insTS desc LIMIT $start, $limit");
	
	   if($_SESSION["usertype"]=="Admin" || $_SESSION["usertype"]=="Supervisor"|| $_SESSION["usertype"]=="Patient"){
	   ?>
	 
<table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

        <td bgcolor="#333333"><div align="center"><strong><span class="style1">Readings </span></strong></div></td>

      </tr>

      <tr>

        <td>&nbsp;</td>

      </tr>

      <tr>

        <td align="center">
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">

          <tr>

        <td width="115"><strong>Date </strong></td>    
		<td width="101"><strong>BMI </strong></td> 
		<td width="99"><strong>Height</strong></td>
           
			<td width="93"><strong>Weight</strong></td>
            <td width="126"><strong>Blood Pressure </strong></td>
			 <td ></td>
          
          </tr>

		  

		  

		  <?php

								while($row = mysql_fetch_array($result))
		{

										 ?>

  											<tr>

<td width="115"><?php echo "<a href='pi1.php?c=pi&id=".$row['id']."'>". $row['date']."</a>"; ?></td>
 <td width="101"><?php echo $row['bmi']; ?></td>
       	
<td width="99"><?php echo $row['height']; ?></td>
      <td width="93"><?php echo $row['weight']; ?></td>
<td width="126"><?php echo $row['bp']; ?></td>
       <td width="57"><a href="pi_edit.php?c=pi_edit&op=u&id=<?php echo $row['id'];?>"><img src="images/edit-icon.png" border="0" alt="delete" /></a></td>

       

							</tr> 


                                             <?php

									  }
									 	
										 ?>

  									


              


        </table></td>

      </tr>
 <tr>

        <td align="center"><div style="margin-left:20px;"><?php echo $pagination; ?></div></td>

      </tr>
	  
     
    </table>
	<?php 
	}
	?>
      
     </form>
     <div align="justify"></div>
<div id="respond"></div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><a href="pi_edit.php?c=pi_edit&acc=<?php echo $id ;?>&pid=<?php  if(isset($_POST["patientId"]) )echo $_POST["patientId"]; else echo  $_SESSION['patientId'];?>"><img src="images/add_info.fw.png" width="294" height="48" /></a> </p>
           </div>    
          <div class="footer-bottom">
            <div class="copy">
		      <p> © All Rights Reserved 2014 BMC</p>
	       </div>	    
	     </div>   
     </div>
  </body>
</html>
<?php } ?>
    	
    	
            