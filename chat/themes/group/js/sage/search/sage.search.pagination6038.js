jQuery.fn.pagination=function(maxentries,opts){opts=jQuery.extend({callbackCalledOnLoad:true,items_per_page:10,num_display_entries:10,current_page:0,num_edge_entries:0,link_to:"#",ellipse_text:"...",prev_show_always:true,next_show_always:true,callback:function(){return false;}},opts||{});return this.each(function(){function numPages(){return Math.ceil(maxentries/opts.items_per_page);}
function getInterval(){var np=numPages();var start=Math.floor(current_page/opts.num_display_entries)*opts.num_display_entries;start=start>np?np:(start<0?0:start);var end=Math.min(start+ opts.num_display_entries,np);return[start,end];}
function pageSelected(page_id,evt){current_page=page_id;drawLinks();var continuePropagation=opts.callback(page_id,panel);if(!continuePropagation){if(evt.stopPropagation){evt.stopPropagation();}
else{evt.cancelBubble=true;}}
return continuePropagation;}
function drawLinks(){panel.empty();var interval=getInterval();var np=numPages();var getClickHandler=function(page_id){return function(evt){return pageSelected(page_id,evt);}};var orederList=jQuery('<ol>');var appendItem=function(page_id,appendopts,disabled){page_id=page_id<0?0:(page_id<np?page_id:np- 1);appendopts=jQuery.extend({text:page_id+ 1,classes:""},appendopts||{});if(page_id==current_page||disabled){var lnk=jQuery("<li><a class='active' href='javascript:void(0);'>"+(appendopts.text)+"</a></li>");}else{var lnk=jQuery("<li><a>"+(appendopts.text)+"</a></li>").bind("click",getClickHandler(page_id)).attr('href',opts.link_to.replace(/__id__/,page_id));}
if(appendopts.classes){lnk.addClass(appendopts.classes);}
orederList.append(lnk);};var appendPreview=function(page_id,disable){page_id=page_id<0?0:(page_id<np?page_id:np- 1);if(!disable){var lnk=jQuery("<div class='delta prev'><i class='icon arrow-left-icon'></i>Previous</div>").bind("click",getClickHandler(page_id));}else{var lnk=jQuery("<div class='delta prev'><i class='icon arrow-left-icon'></i>Previous</div>");}
panel.append(lnk);};var appendNext=function(page_id,diable){page_id=page_id<0?0:(page_id<np?page_id:np- 1);if(!diable)
{var lnk=jQuery("<div class='delta next'><i class='icon arrow-right-icon'></i>Next</div>").bind("click",getClickHandler(page_id));}else{var lnk=jQuery("<div class='delta next'><i class='icon arrow-right-icon'></i>Next</div>");}
panel.append(lnk);};if(current_page>0){appendPreview(current_page- 1,false);}else{appendPreview(current_page- 1,true);}
for(var i=interval[0];i<interval[1];i++){appendItem(i);}
panel.append(orederList);if(current_page<np- 1){appendNext(current_page+ 1,false);}else{appendNext(current_page+ 1,true);}}
var current_page=opts.current_page;maxentries=(!maxentries||maxentries<0)?1:maxentries;opts.items_per_page=(!opts.items_per_page||opts.items_per_page<0)?1:opts.items_per_page;var panel=jQuery(this);this.selectPage=function(page_id){pageSelected(page_id);}
this.prevPage=function(){if(current_page>0){pageSelected(current_page- 1);return true;}
else{return false;}}
this.nextPage=function(){if(current_page<numPages()- 1){pageSelected(current_page+ 1);return true;}
else{return false;}}
drawLinks();if(opts.callbackCalledOnLoad){opts.callback(current_page,this);}});}