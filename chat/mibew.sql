-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 14, 2015 at 10:34 AM
-- Server version: 5.1.41
-- PHP Version: 5.3.1



/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mibew`
--

-- --------------------------------------------------------

--
-- Table structure for table `chatban`
--

CREATE TABLE IF NOT EXISTS `chatban` (
  `banid` int(11) NOT NULL AUTO_INCREMENT,
  `dtmcreated` datetime DEFAULT '0000-00-00 00:00:00',
  `dtmtill` datetime DEFAULT '0000-00-00 00:00:00',
  `address` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `blockedCount` int(11) DEFAULT '0',
  PRIMARY KEY (`banid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

--
-- Dumping data for table `chatban`
--


-- --------------------------------------------------------

--
-- Table structure for table `chatconfig`
--

CREATE TABLE IF NOT EXISTS `chatconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vckey` varchar(255) DEFAULT NULL,
  `vcvalue` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  AUTO_INCREMENT=34 ;

--
-- Dumping data for table `chatconfig`
--

INSERT INTO `chatconfig` (`id`, `vckey`, `vcvalue`) VALUES
(1, 'dbversion', '1.6.10'),
(2, 'featuresversion', '1.6.6'),
(3, 'title', 'Belvedere Medical Centre'),
(4, 'hosturl', 'http://www.test.org'),
(5, 'logo', ''),
(6, 'usernamepattern', '{name}'),
(7, 'chatstyle', 'silver'),
(8, 'chattitle', 'Live Doctor Support'),
(9, 'geolink', 'http://api.hostip.info/get_html.php?ip={ip}'),
(10, 'geolinkparams', 'width=440,height=100,toolbar=0,scrollbars=0,location=0,status=1,menubar=0,resizable=1'),
(11, 'max_connections_from_one_host', '10'),
(12, 'thread_lifetime', '600'),
(13, 'email', ''),
(14, 'sendmessagekey', 'center'),
(15, 'enableban', '0'),
(16, 'enablessl', '0'),
(17, 'forcessl', '0'),
(18, 'usercanchangename', '1'),
(19, 'enablegroups', '0'),
(20, 'enablestatistics', '1'),
(21, 'enablejabber', '1'),
(22, 'enablepresurvey', '1'),
(23, 'surveyaskmail', '0'),
(24, 'surveyaskgroup', '1'),
(25, 'surveyaskmessage', '0'),
(26, 'surveyaskcaptcha', '0'),
(27, 'enablepopupnotification', '1'),
(28, 'showonlineoperators', '1'),
(29, 'enablecaptcha', '0'),
(30, 'online_timeout', '30'),
(31, 'updatefrequency_operator', '2'),
(32, 'updatefrequency_chat', '2'),
(33, 'updatefrequency_oldchat', '7');

-- --------------------------------------------------------

--
-- Table structure for table `chatgroup`
--

CREATE TABLE IF NOT EXISTS `chatgroup` (
  `groupid` int(11) NOT NULL AUTO_INCREMENT,
  `vcemail` varchar(64) DEFAULT NULL,
  `vclocalname` varchar(64) NOT NULL,
  `vccommonname` varchar(64) NOT NULL,
  `vclocaldescription` varchar(1024) NOT NULL,
  `vccommondescription` varchar(1024) NOT NULL,
  PRIMARY KEY (`groupid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

--
-- Dumping data for table `chatgroup`
--


-- --------------------------------------------------------

--
-- Table structure for table `chatgroupoperator`
--

CREATE TABLE IF NOT EXISTS `chatgroupoperator` (
  `groupid` int(11) NOT NULL,
  `operatorid` int(11) NOT NULL
) ENGINE=InnoDB;

--
-- Dumping data for table `chatgroupoperator`
--


-- --------------------------------------------------------

--
-- Table structure for table `chatmessage`
--

CREATE TABLE IF NOT EXISTS `chatmessage` (
  `messageid` int(11) NOT NULL AUTO_INCREMENT,
  `threadid` int(11) NOT NULL,
  `ikind` int(11) NOT NULL,
  `agentId` int(11) NOT NULL DEFAULT '0',
  `tmessage` text NOT NULL,
  `dtmcreated` datetime DEFAULT '0000-00-00 00:00:00',
  `tname` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`messageid`),
  KEY `idx_agentid` (`agentId`)
) ENGINE=InnoDB  AUTO_INCREMENT=198 ;

--
-- Dumping data for table `chatmessage`
--

INSERT INTO `chatmessage` (`messageid`, `threadid`, `ikind`, `agentId`, `tmessage`, `dtmcreated`, `tname`) VALUES
(1, 1, 3, 0, 'Vistor came from page http://localhost/mibew/operator/getcode.php\nhttp://localhost/mibew/operator/operators.php', '2015-02-12 15:38:15', NULL),
(2, 1, 4, 0, 'Thank you for contacting us. An operator will be with you shortly.', '2015-02-12 15:38:15', NULL),
(3, 1, 1, 0, 'ji', '2015-02-12 15:38:21', 'Guest'),
(4, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 15:38:25', NULL),
(5, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 15:38:28', NULL),
(6, 1, 6, 0, 'Visitor joined chat again', '2015-02-12 15:38:29', NULL),
(7, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 15:38:38', NULL),
(8, 1, 6, 0, 'Visitor joined chat again', '2015-02-12 15:38:39', NULL),
(9, 1, 1, 0, 'gigigig\n\n', '2015-02-12 15:38:47', 'Guest'),
(10, 1, 1, 0, 'ghghg\n', '2015-02-12 15:39:12', 'Guest'),
(11, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 15:39:27', NULL),
(12, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 15:39:29', NULL),
(13, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 15:39:29', NULL),
(14, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 15:39:29', NULL),
(15, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 15:39:29', NULL),
(16, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 15:39:29', NULL),
(17, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 15:39:29', NULL),
(18, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 15:39:30', NULL),
(19, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 15:39:30', NULL),
(20, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 15:39:30', NULL),
(21, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 15:39:30', NULL),
(22, 1, 6, 0, 'Visitor joined chat again', '2015-02-12 15:39:34', NULL),
(23, 1, 6, 0, 'Operator Administrator joined the chat', '2015-02-12 15:39:54', NULL),
(24, 1, 2, 1, 'hi how are you\n', '2015-02-12 15:40:22', 'Administrator'),
(25, 1, 1, 0, 'am very well. How can l assist', '2015-02-12 15:40:40', 'Guest'),
(26, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 15:41:13', NULL),
(27, 1, 6, 0, 'Visitor joined chat again', '2015-02-12 15:41:14', NULL),
(28, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 15:41:49', NULL),
(29, 1, 5, 0, 'Your operator has connection issues. We have moved you to a priorty position in the queue. Sorry for keeping you waiting.', '2015-02-12 15:42:00', NULL),
(30, 1, 6, 0, 'The visitor changed their name Guest to memory', '2015-02-12 15:42:20', NULL),
(31, 1, 6, 0, 'Operator Administrator is back', '2015-02-12 15:42:25', NULL),
(32, 1, 2, 1, 'Hello. How may I help you?', '2015-02-12 15:42:34', 'Administrator'),
(33, 1, 5, 0, 'Your operator has connection issues. We have moved you to a priorty position in the queue. Sorry for keeping you waiting.', '2015-02-12 15:43:08', NULL),
(34, 1, 1, 0, 'hjhj', '2015-02-12 17:24:17', 'memory'),
(35, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 17:24:36', NULL),
(36, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 17:24:44', NULL),
(37, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 17:24:50', NULL),
(38, 1, 6, 0, 'Visitor joined chat again', '2015-02-12 17:24:55', NULL),
(39, 2, 3, 0, 'Vistor came from page http://192.168.1.67/voitionog/chat.php', '2015-02-12 17:28:16', NULL),
(40, 2, 4, 0, 'Thank you for contacting us. An operator will be with you shortly.', '2015-02-12 17:28:16', NULL),
(41, 2, 1, 0, 'hi', '2015-02-12 17:28:43', 'simbast'),
(42, 2, 6, 0, 'Operator Administrator joined the chat', '2015-02-12 17:29:04', NULL),
(43, 2, 2, 1, 'hi sir how are uou\n', '2015-02-12 17:29:17', 'Administrator'),
(44, 2, 1, 0, 'great tnx', '2015-02-12 17:29:38', 'simbast'),
(45, 2, 6, 0, 'Operator Administrator left the chat', '2015-02-12 17:29:49', NULL),
(46, 1, 6, 0, 'Operator Administrator is back', '2015-02-12 17:30:00', NULL),
(47, 1, 6, 0, 'Visitor joined chat again', '2015-02-12 17:35:56', NULL),
(48, 1, 6, 0, 'Visitor joined chat again', '2015-02-12 17:38:56', NULL),
(49, 3, 3, 0, 'Vistor came from page http://192.168.1.67/voitionog/chat.php', '2015-02-12 17:40:19', NULL),
(50, 3, 4, 0, 'Thank you for contacting us. An operator will be with you shortly.', '2015-02-12 17:40:19', NULL),
(51, 3, 6, 0, 'Operator Administrator joined the chat', '2015-02-12 17:41:07', NULL),
(52, 3, 2, 1, 'Hello. How may I help you?', '2015-02-12 17:41:40', 'Administrator'),
(53, 1, 6, 0, 'Visitor joined chat again', '2015-02-12 17:42:10', NULL),
(54, 1, 1, 0, 'frffrr\n', '2015-02-12 17:42:16', 'memory'),
(55, 3, 3, 0, 'Visitor closed chat window', '2015-02-12 17:42:27', NULL),
(56, 3, 6, 0, 'Operator root changed operator Administrator', '2015-02-12 17:46:06', NULL),
(57, 1, 6, 0, 'Operator root changed operator Administrator', '2015-02-12 17:46:12', NULL),
(58, 1, 5, 0, 'Your operator has connection issues. We have moved you to a priorty position in the queue. Sorry for keeping you waiting.', '2015-02-12 17:46:49', NULL),
(59, 1, 6, 0, 'Operator root is back', '2015-02-12 17:47:03', NULL),
(60, 1, 5, 0, 'Your operator has connection issues. We have moved you to a priorty position in the queue. Sorry for keeping you waiting.', '2015-02-12 17:47:36', NULL),
(61, 1, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 21:39:19', NULL),
(62, 1, 6, 0, 'Visitor joined chat again', '2015-02-12 21:51:20', NULL),
(63, 1, 6, 0, 'Visitor joined chat again', '2015-02-12 22:07:45', NULL),
(64, 3, 2, 2, 'Hello. How may I help you?', '2015-02-12 22:09:10', 'root'),
(65, 4, 3, 0, 'Vistor came from page http://192.168.1.68/voitionog/chat.php', '2015-02-12 22:11:45', NULL),
(66, 4, 4, 0, 'Thank you for contacting us. An operator will be with you shortly.', '2015-02-12 22:11:45', NULL),
(67, 3, 6, 0, 'Operator root left the chat', '2015-02-12 22:12:16', NULL),
(68, 4, 6, 0, 'Operator root joined the chat', '2015-02-12 22:12:20', NULL),
(69, 4, 2, 2, 'Hello! Welcome to our support. How may I help you?', '2015-02-12 22:12:23', 'root'),
(70, 4, 3, 0, 'Visitor closed chat window', '2015-02-12 22:13:15', NULL),
(71, 5, 3, 0, 'Vistor came from page http://192.168.1.68/voitionog/chat.php', '2015-02-12 22:14:01', NULL),
(72, 5, 4, 0, 'Thank you for contacting us. An operator will be with you shortly.', '2015-02-12 22:14:01', NULL),
(73, 1, 6, 0, 'Visitor memory left the chat', '2015-02-12 22:15:43', NULL),
(74, 1, 6, 0, 'Visitor memory left the chat', '2015-02-12 22:15:49', NULL),
(75, 4, 6, 0, 'Operator root left the chat', '2015-02-12 22:16:04', NULL),
(76, 6, 3, 0, 'Vistor came from page http://192.168.1.68/voitionog/chat.php', '2015-02-12 22:16:44', NULL),
(77, 6, 4, 0, 'Thank you for contacting us. An operator will be with you shortly.', '2015-02-12 22:16:44', NULL),
(78, 6, 6, 0, 'Operator root joined the chat', '2015-02-12 22:16:48', NULL),
(79, 6, 6, 0, 'Operator root left the chat', '2015-02-12 22:16:50', NULL),
(80, 5, 6, 0, 'Operator root joined the chat', '2015-02-12 22:16:54', NULL),
(81, 5, 3, 0, 'Visitor closed chat window', '2015-02-12 22:14:52', NULL),
(82, 5, 6, 0, 'Operator root left the chat', '2015-02-12 22:17:09', NULL),
(83, 7, 3, 0, 'Vistor came from page http://192.168.1.68/voitionog/chat.php', '2015-02-12 22:25:47', NULL),
(84, 7, 4, 0, 'Thank you for contacting us. An operator will be with you shortly.', '2015-02-12 22:25:47', NULL),
(85, 7, 6, 0, 'Operator Administrator joined the chat', '2015-02-12 22:25:57', NULL),
(86, 7, 7, 0, '/mibew/images/avatar/1.jpg', '2015-02-12 22:25:57', NULL),
(87, 7, 3, 0, 'Visitor closed chat window', '2015-02-12 22:26:39', NULL),
(88, 8, 3, 0, 'Vistor came from page http://localhost/voitionog/chat.php', '2015-02-12 22:38:56', NULL),
(89, 8, 4, 0, 'Thank you for contacting us. An operator will be with you shortly.', '2015-02-12 22:38:56', NULL),
(90, 8, 6, 0, 'Operator Administrator joined the chat', '2015-02-12 22:39:05', NULL),
(91, 8, 7, 0, '/mibew/images/avatar/1.jpg', '2015-02-12 22:39:05', NULL),
(92, 7, 3, 0, 'Visitor closed chat window', '2015-02-12 22:39:21', NULL),
(93, 8, 6, 0, 'Visitor memory left the chat', '2015-02-12 22:43:30', NULL),
(94, 9, 3, 0, 'Vistor came from page http://localhost/voitionog/chat.php', '2015-02-12 22:43:57', NULL),
(95, 9, 4, 0, 'Thank you for contacting us. An operator will be with you shortly.', '2015-02-12 22:43:58', NULL),
(96, 9, 6, 0, 'Operator Administrator joined the chat', '2015-02-12 22:44:03', NULL),
(97, 9, 7, 0, '/mibew/images/avatar/1.jpg', '2015-02-12 22:44:03', NULL),
(98, 9, 2, 1, 'hi\n', '2015-02-12 22:44:09', 'Administrator'),
(99, 9, 1, 0, 'koko', '2015-02-12 22:44:29', 'memory'),
(100, 7, 3, 0, 'Visitor closed chat window', '2015-02-12 22:48:00', NULL),
(101, 9, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 22:54:43', NULL),
(102, 9, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-12 22:55:38', NULL),
(103, 9, 6, 0, 'Operator Administrator left the chat', '2015-02-12 23:00:16', NULL),
(104, 7, 6, 0, 'Operator Administrator left the chat', '2015-02-12 23:00:21', NULL),
(105, 10, 3, 0, 'Vistor came from page http://192.168.1.68/voitionog/chat.php', '2015-02-12 23:13:58', NULL),
(106, 10, 4, 0, 'Thank you for contacting us. An operator will be with you shortly.', '2015-02-12 23:13:58', NULL),
(107, 10, 6, 0, 'Operator Administrator joined the chat', '2015-02-12 23:15:46', NULL),
(108, 10, 7, 0, '/mibew/images/avatar/1.jpg', '2015-02-12 23:15:46', NULL),
(109, 10, 2, 1, 'hi', '2015-02-12 23:16:15', 'Administrator'),
(110, 10, 2, 1, 'javascript:void(0)javascript:void(0)', '2015-02-12 23:47:24', 'Administrator'),
(111, 10, 6, 0, 'Visitor joined chat again', '2015-02-12 23:50:54', NULL),
(112, 10, 5, 0, 'Your operator has connection issues. We have moved you to a priorty position in the queue. Sorry for keeping you waiting.', '2015-02-12 23:51:18', NULL),
(113, 10, 6, 0, 'Operator Administrator is back', '2015-02-12 23:51:46', NULL),
(114, 10, 7, 0, '/mibew/images/avatar/1.jpg', '2015-02-12 23:51:46', NULL),
(115, 10, 3, 0, 'Visitor closed chat window', '2015-02-12 23:52:29', NULL),
(116, 11, 3, 0, 'Vistor came from page http://192.168.1.68/voitionog/chat.php', '2015-02-13 00:10:26', NULL),
(117, 11, 4, 0, 'Thank you for contacting us. An operator will be with you shortly.', '2015-02-13 00:10:26', NULL),
(118, 11, 1, 0, 'Bhj', '2015-02-13 00:10:43', 'Guest'),
(119, 11, 1, 0, 'hrhhhs', '2015-02-13 00:11:05', 'Guest'),
(120, 11, 6, 0, 'Operator Administrator joined the chat', '2015-02-13 00:11:23', NULL),
(121, 11, 7, 0, '/mibew/images/avatar/1.jpg', '2015-02-13 00:11:23', NULL),
(122, 11, 5, 0, 'Your operator has connection issues. We have moved you to a priorty position in the queue. Sorry for keeping you waiting.', '2015-02-13 00:11:58', NULL),
(123, 11, 6, 0, 'Operator Administrator is back', '2015-02-13 23:11:42', NULL),
(124, 11, 7, 0, '/mibew/images/avatar/1.jpg', '2015-02-13 23:11:42', NULL),
(125, 11, 3, 0, 'Visitor closed chat window', '2015-02-13 00:41:59', NULL),
(126, 11, 6, 0, 'Operator Administrator left the chat', '2015-02-13 23:11:44', NULL),
(127, 10, 6, 0, 'Operator Administrator left the chat', '2015-02-13 23:59:40', NULL),
(128, 12, 3, 0, 'Vistor came from page http://localhost/voitionog/chat.php', '2015-02-14 00:00:25', NULL),
(129, 12, 4, 0, 'Thank you for contacting us. An operator will be with you shortly.', '2015-02-14 00:00:25', NULL),
(130, 12, 6, 0, 'Operator Administrator joined the chat', '2015-02-14 00:23:27', NULL),
(131, 12, 7, 0, '/mibew/images/avatar/1.jpg', '2015-02-14 00:23:27', NULL),
(132, 12, 3, 0, 'Visitor closed chat window', '2015-02-14 00:23:45', NULL),
(133, 12, 3, 0, 'Visitor navigated to http://localhost/voitionog/chat.php', '2015-02-14 00:28:03', NULL),
(134, 12, 6, 0, 'Visitor joined chat again', '2015-02-14 00:28:04', NULL),
(135, 12, 5, 0, 'Your operator has connection issues. We have moved you to a priorty position in the queue. Sorry for keeping you waiting.', '2015-02-14 00:24:46', NULL),
(136, 12, 6, 0, 'Operator Administrator is back', '2015-02-14 00:28:33', NULL),
(137, 12, 7, 0, '/mibew/images/avatar/1.jpg', '2015-02-14 00:28:33', NULL),
(138, 12, 6, 0, 'Operator Administrator left the chat', '2015-02-14 00:30:11', NULL),
(139, 13, 3, 0, 'Vistor came from page http://localhost/voitionog/chat.php', '2015-02-14 00:30:34', NULL),
(140, 13, 4, 0, 'Thank you for contacting us. One of our available Doctors will be with you shortly.', '2015-02-14 00:30:34', NULL),
(141, 13, 6, 0, 'Operator Administrator joined the chat', '2015-02-14 00:30:41', NULL),
(142, 13, 7, 0, '/mibew/images/avatar/1.jpg', '2015-02-14 00:30:41', NULL),
(143, 13, 6, 0, 'Visitor Susan left the chat', '2015-02-14 00:33:26', NULL),
(144, 13, 6, 0, 'Doctor Administrator left the chat', '2015-02-14 00:33:30', NULL),
(145, 14, 3, 0, 'Vistor came from page http://localhost/voitionog/chat.php', '2015-02-14 00:33:46', NULL),
(146, 14, 4, 0, 'Thank you for contacting us. One of our available Doctors will be with you shortly.', '2015-02-14 00:33:46', NULL),
(147, 14, 6, 0, 'Doctor Administrator joined the chat', '2015-02-14 00:33:49', NULL),
(148, 14, 7, 0, '/mibew/images/avatar/1.jpg', '2015-02-14 00:33:49', NULL),
(149, 14, 2, 1, 'hi Susan', '2015-02-14 00:34:12', 'Administrator'),
(150, 14, 1, 0, 'rrrrrrrrrrrrrrrrrrrrrrrrrrr', '2015-02-14 00:36:04', 'Susan'),
(151, 14, 2, 1, 'reerrererrrrrrrr', '2015-02-14 00:36:18', 'Administrator'),
(152, 14, 2, 1, 'Hello! Welcome to our support. How may I help you?', '2015-02-14 00:38:40', 'Administrator'),
(153, 14, 1, 0, 'jiji', '2015-02-14 00:38:50', 'Susan'),
(154, 14, 6, 0, 'Doctor Administrator redirected you to another Doctor. Please wait a while.', '2015-02-14 00:46:38', NULL),
(155, 14, 6, 0, 'Doctor Doctor Macheka changed Doctor Administrator', '2015-02-14 00:46:54', NULL),
(156, 14, 2, 3, ';k;', '2015-02-14 00:48:39', 'Doctor Macheka'),
(157, 14, 6, 0, 'Doctor Doctor Macheka left the chat', '2015-02-14 00:54:11', NULL),
(158, 15, 3, 0, 'Vistor came from page http://localhost/voitionog/chat.php', '2015-02-14 00:54:58', NULL),
(159, 15, 4, 0, 'Thank you for contacting us. One of our available Doctors will be with you shortly.', '2015-02-14 00:54:58', NULL),
(160, 15, 6, 0, 'Doctor Macheka joined the chat', '2015-02-14 00:56:26', NULL),
(161, 15, 2, 3, 'hi', '2015-02-14 00:56:40', 'Doctor Macheka'),
(162, 15, 6, 0, 'Doctor Macheka redirected you to another Doctor. Please wait a while.', '2015-02-14 00:56:49', NULL),
(163, 15, 6, 0, 'Administrator changed  Doctor Macheka', '2015-02-14 00:57:03', NULL),
(164, 15, 7, 0, '/mibew/images/avatar/1.jpg', '2015-02-14 00:57:03', NULL),
(165, 15, 2, 1, 'helo', '2015-02-14 00:57:18', 'Administrator'),
(166, 15, 6, 0, 'Visitor joined chat again', '2015-02-14 01:16:47', NULL),
(167, 15, 6, 0, 'Visitor joined chat again', '2015-02-14 01:18:31', NULL),
(168, 15, 6, 0, 'Visitor Susan left the chat', '2015-02-14 01:19:19', NULL),
(169, 16, 3, 0, 'Vistor came from page http://localhost/voitionog/chat.php', '2015-02-14 01:19:45', NULL),
(170, 16, 4, 0, 'Thank you for contacting us. One of our available Doctors will be with you shortly.', '2015-02-14 01:19:45', NULL),
(171, 15, 3, 0, 'Visitor closed chat window', '2015-02-14 01:19:50', NULL),
(172, 16, 6, 0, 'Visitor Susan left the chat', '2015-02-14 01:19:54', NULL),
(173, 15, 6, 0, 'Administrator left the chat', '2015-02-14 01:20:28', NULL),
(174, 17, 3, 0, 'Vistor came from page http://localhost/voitionog/chat.php', '2015-02-14 01:20:39', NULL),
(175, 17, 4, 0, 'Thank you for contacting us. One of our available Doctors will be with you shortly.', '2015-02-14 01:20:39', NULL),
(176, 17, 6, 0, 'Doctor Macheka joined the chat', '2015-02-14 01:20:48', NULL),
(177, 17, 7, 0, '/bmc/images/avatar/3.jpg', '2015-02-14 01:20:48', NULL),
(178, 17, 3, 0, 'Visitor closed chat window', '2015-02-14 01:26:23', NULL),
(179, 18, 3, 0, 'Vistor came from page http://localhost/voitionog/chat.php', '2015-02-14 09:57:38', NULL),
(180, 18, 4, 0, 'Thank you for contacting us. One of our available Doctors will be with you shortly.', '2015-02-14 09:57:38', NULL),
(181, 18, 6, 0, 'Administrator joined the chat', '2015-02-14 09:57:43', NULL),
(182, 18, 7, 0, '/bmc/images/avatar/1.jpg', '2015-02-14 09:57:43', NULL),
(183, 19, 3, 0, 'Vistor came from page http://192.168.1.68/voitionog/chat.php', '2015-02-14 10:19:08', NULL),
(184, 19, 4, 0, 'Thank you for contacting us. One of our available Doctors will be with you shortly.', '2015-02-14 10:19:08', NULL),
(185, 19, 6, 0, 'Administrator joined the chat', '2015-02-14 10:19:19', NULL),
(186, 19, 7, 0, '/bmc/images/avatar/1.jpg', '2015-02-14 10:19:19', NULL),
(187, 19, 1, 0, 'Musa \n', '2015-02-14 10:19:40', 'Guest'),
(188, 19, 6, 0, 'Administrator redirected you to another Doctor. Please wait a while.', '2015-02-14 10:20:05', NULL),
(189, 19, 6, 0, 'Doctor Macheka changed  Administrator', '2015-02-14 10:20:14', NULL),
(190, 19, 7, 0, '/bmc/images/avatar/3.jpg', '2015-02-14 10:20:14', NULL),
(191, 19, 2, 3, 'Hello', '2015-02-14 10:20:23', 'Doctor Macheka'),
(192, 18, 5, 0, 'Your Doctor has connection issues. We have moved you to a priorty position in the queue. Sorry for keeping you waiting.', '2015-02-14 10:20:36', NULL),
(193, 19, 1, 0, 'Do u have a phd', '2015-02-14 10:21:05', 'Guest'),
(194, 19, 2, 3, 'Standard', '2015-02-14 10:21:21', 'Doctor Macheka'),
(195, 18, 6, 0, 'Administrator is back', '2015-02-14 10:25:41', NULL),
(196, 18, 7, 0, '/bmc/images/avatar/1.jpg', '2015-02-14 10:25:41', NULL),
(197, 18, 3, 0, 'Visitor closed chat window', '2015-02-14 10:29:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `chatnotification`
--

CREATE TABLE IF NOT EXISTS `chatnotification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(8) DEFAULT NULL,
  `vckind` varchar(16) DEFAULT NULL,
  `vcto` varchar(256) DEFAULT NULL,
  `dtmcreated` datetime DEFAULT '0000-00-00 00:00:00',
  `vcsubject` varchar(256) DEFAULT NULL,
  `tmessage` text NOT NULL,
  `refoperator` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  AUTO_INCREMENT=2 ;

--
-- Dumping data for table `chatnotification`
--

INSERT INTO `chatnotification` (`id`, `locale`, `vckind`, `vcto`, `dtmcreated`, `vcsubject`, `tmessage`, `refoperator`) VALUES
(1, 'en', 'mail', 'sds@sdsd.com', '2015-02-14 00:40:31', 'Mibew: dialog history', 'Hello Susan!\r\n\r\nYour chat history: \r\n\r\n00:34:10 Thank you for contacting us. One of our available Doctors will be with you shortly.\r\n00:34:13 [Doctor Administrator joined the chat]\r\n00:34:36 Administrator: hi Susan\r\n00:36:28 Susan: rrrrrrrrrrrrrrrrrrrrrrrrrrr\r\n00:36:42 Administrator: reerrererrrrrrrr\r\n00:39:04 Administrator: Hello! Welcome to our support. How may I help you?\r\n00:39:14 Susan: jiji\r\n\r\n--- \r\nRegards,\r\nMibew', 0);

-- --------------------------------------------------------

--
-- Table structure for table `chatoperator`
--

CREATE TABLE IF NOT EXISTS `chatoperator` (
  `operatorid` int(11) NOT NULL AUTO_INCREMENT,
  `vclogin` varchar(64) NOT NULL,
  `vcpassword` varchar(64) NOT NULL,
  `vclocalename` varchar(64) NOT NULL,
  `vccommonname` varchar(64) NOT NULL,
  `vcemail` varchar(64) DEFAULT NULL,
  `dtmlastvisited` datetime DEFAULT '0000-00-00 00:00:00',
  `istatus` int(11) DEFAULT '0',
  `vcavatar` varchar(255) DEFAULT NULL,
  `vcjabbername` varchar(255) DEFAULT NULL,
  `iperm` int(11) DEFAULT '65535',
  `inotify` int(11) DEFAULT '0',
  `dtmrestore` datetime DEFAULT '0000-00-00 00:00:00',
  `vcrestoretoken` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`operatorid`)
) ENGINE=InnoDB  AUTO_INCREMENT=4 ;

--
-- Dumping data for table `chatoperator`
--

INSERT INTO `chatoperator` (`operatorid`, `vclogin`, `vcpassword`, `vclocalename`, `vccommonname`, `vcemail`, `dtmlastvisited`, `istatus`, `vcavatar`, `vcjabbername`, `iperm`, `inotify`, `dtmrestore`, `vcrestoretoken`) VALUES
(1, 'admin', '$2a$08$6GAj8LdLdOR1WIcCGc.TkuyM3oRT..FUmtrfD0K5TQub3BSybvJLW', 'Administrator', 'Administrator', '', '2015-02-14 10:33:49', 0, '/bmc/images/avatar/1.jpg', '', 65535, 0, '0000-00-00 00:00:00', NULL),
(2, 'root', '$2a$08$MubkubitU3/tOqGSVIUMF.L3E2WXSDMm2WjQ5Bexva.BIza8VGaB2', 'root', 'Root User', '', '2015-02-12 22:17:18', 0, '', '', 65530, 0, '0000-00-00 00:00:00', NULL),
(3, 'Macheka', '$2a$08$quisTW7u6t787SQPOIaLSujCkXgpVdYSnGE9jvu56MeSOKZWlJIbi', 'Doctor Macheka', 'Macheka', '', '2015-02-14 10:22:12', 0, '/bmc/images/avatar/3.jpg', '', 65534, 0, '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `chatresponses`
--

CREATE TABLE IF NOT EXISTS `chatresponses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(8) DEFAULT NULL,
  `groupid` int(11) DEFAULT NULL,
  `vcvalue` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  AUTO_INCREMENT=3 ;

--
-- Dumping data for table `chatresponses`
--

INSERT INTO `chatresponses` (`id`, `locale`, `groupid`, `vcvalue`) VALUES
(1, 'en', NULL, 'Hello. How may I help you?'),
(2, 'en', NULL, 'Hello! Welcome to our Live Doctor Support. How may I help you?');

-- --------------------------------------------------------

--
-- Table structure for table `chatrevision`
--

CREATE TABLE IF NOT EXISTS `chatrevision` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB;

--
-- Dumping data for table `chatrevision`
--

INSERT INTO `chatrevision` (`id`) VALUES
(6653);

-- --------------------------------------------------------

--
-- Table structure for table `chatthread`
--

CREATE TABLE IF NOT EXISTS `chatthread` (
  `threadid` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(64) NOT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `agentName` varchar(64) DEFAULT NULL,
  `agentId` int(11) NOT NULL DEFAULT '0',
  `dtmcreated` datetime DEFAULT '0000-00-00 00:00:00',
  `dtmmodified` datetime DEFAULT '0000-00-00 00:00:00',
  `lrevision` int(11) NOT NULL DEFAULT '0',
  `istate` int(11) NOT NULL DEFAULT '0',
  `ltoken` int(11) NOT NULL,
  `remote` varchar(255) DEFAULT NULL,
  `referer` text,
  `nextagent` int(11) NOT NULL DEFAULT '0',
  `locale` varchar(8) DEFAULT NULL,
  `lastpinguser` datetime DEFAULT '0000-00-00 00:00:00',
  `lastpingagent` datetime DEFAULT '0000-00-00 00:00:00',
  `userTyping` int(11) DEFAULT '0',
  `agentTyping` int(11) DEFAULT '0',
  `shownmessageid` int(11) NOT NULL DEFAULT '0',
  `userAgent` varchar(255) DEFAULT NULL,
  `messageCount` varchar(16) DEFAULT NULL,
  `groupid` int(11) DEFAULT NULL,
  PRIMARY KEY (`threadid`)
) ENGINE=InnoDB  AUTO_INCREMENT=20 ;

--
-- Dumping data for table `chatthread`
--

INSERT INTO `chatthread` (`threadid`, `userName`, `userid`, `agentName`, `agentId`, `dtmcreated`, `dtmmodified`, `lrevision`, `istate`, `ltoken`, `remote`, `referer`, `nextagent`, `locale`, `lastpinguser`, `lastpingagent`, `userTyping`, `agentTyping`, `shownmessageid`, `userAgent`, `messageCount`, `groupid`) VALUES
(1, 'memory', '54dcac7a5cfee9.96040303', 'root', 2, '2015-02-12 15:38:15', '2015-02-12 22:15:43', 717, 3, 519839, '::1', 'http://localhost/mibew/operator/getcode.php\nhttp://localhost/mibew/operator/operators.php', 0, 'en', '2015-02-12 22:15:51', '0000-00-00 00:00:00', 0, 0, 3, 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36', '6', NULL),
(2, 'simbast', '54dcc6994581c3.08226175', 'Administrator', 1, '2015-02-12 17:28:16', '2015-02-12 17:29:49', 239, 3, 4849923, '192.168.1.65', 'http://192.168.1.67/voitionog/chat.php', 0, 'en', '2015-02-12 17:30:37', '0000-00-00 00:00:00', 0, 0, 41, 'Mozilla/5.0 (Symbian/3; Series60/5.3 NokiaE6-00/111.140.0058; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/535.1 (KHTML, like Gecko) NokiaBrowser/8.3.1.4 Mobile Safari/535.1 3gpp-gba', '2', NULL),
(3, 'simbast', '54dcc6994581c3.08226175', 'root', 2, '2015-02-12 17:40:19', '2015-02-12 22:12:16', 627, 3, 13530733, '192.168.1.65', 'http://192.168.1.67/voitionog/chat.php', 0, 'en', '0000-00-00 00:00:00', '2015-02-12 22:12:17', 0, 0, 0, 'Mozilla/5.0 (Symbian/3; Series60/5.3 NokiaE6-00/111.140.0058; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/535.1 (KHTML, like Gecko) NokiaBrowser/8.3.1.4 Mobile Safari/535.1 3gpp-gba', '0', NULL),
(4, 'Guest', '54dd04b3c9c695.81667224', 'root', 2, '2015-02-12 22:11:45', '2015-02-12 22:16:04', 726, 3, 15144399, '192.168.1.67', 'http://192.168.1.68/voitionog/chat.php', 0, 'en', '0000-00-00 00:00:00', '2015-02-12 22:16:05', 0, 0, 0, 'Mozilla/5.0 (Symbian/3; Series60/5.3 NokiaE6-00/111.140.0058; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/535.1 (KHTML, like Gecko) NokiaBrowser/8.3.1.4 Mobile Safari/535.1 3gpp-gba', '0', NULL),
(5, 'Guest', '54dd04b3c9c695.81667224', 'root', 2, '2015-02-12 22:14:01', '2015-02-12 22:17:09', 756, 3, 9445044, '192.168.1.67', 'http://192.168.1.68/voitionog/chat.php', 0, 'en', '0000-00-00 00:00:00', '2015-02-12 22:17:10', 0, 0, 0, 'Mozilla/5.0 (Symbian/3; Series60/5.3 NokiaE6-00/111.140.0058; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/535.1 (KHTML, like Gecko) NokiaBrowser/8.3.1.4 Mobile Safari/535.1 3gpp-gba', '0', NULL),
(6, 'Guest', '54dd04b3c9c695.81667224', 'root', 2, '2015-02-12 22:16:44', '2015-02-12 22:16:50', 745, 3, 13955555, '192.168.1.67', 'http://192.168.1.68/voitionog/chat.php', 0, 'en', '2015-02-12 22:24:51', '0000-00-00 00:00:00', 0, 0, 0, 'Mozilla/5.0 (Symbian/3; Series60/5.3 NokiaE6-00/111.140.0058; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/535.1 (KHTML, like Gecko) NokiaBrowser/8.3.1.4 Mobile Safari/535.1 3gpp-gba', '0', NULL),
(7, 'Guest', '54dd04b3c9c695.81667224', 'Administrator', 1, '2015-02-12 22:25:47', '2015-02-12 23:00:21', 1387, 3, 14986837, '192.168.1.67', 'http://192.168.1.68/voitionog/chat.php', 0, 'en', '0000-00-00 00:00:00', '2015-02-12 23:00:21', 1, 0, 0, 'Mozilla/5.0 (Symbian/3; Series60/5.3 NokiaE6-00/111.140.0058; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/535.1 (KHTML, like Gecko) NokiaBrowser/8.3.1.4 Mobile Safari/535.1 3gpp-gba', '0', NULL),
(8, 'memory', '54dcac7a5cfee9.96040303', 'Administrator', 1, '2015-02-12 22:38:56', '2015-02-12 22:43:29', 1180, 3, 7281257, '::1', 'http://localhost/voitionog/chat.php', 0, 'en', '2015-02-12 22:43:30', '2015-02-12 22:43:45', 0, 0, 0, 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36', '0', NULL),
(9, 'memory', '54dcac7a5cfee9.96040303', 'Administrator', 1, '2015-02-12 22:43:57', '2015-02-12 23:00:16', 1384, 3, 16280892, '::1', 'http://localhost/voitionog/chat.php', 0, 'en', '2015-02-12 22:59:54', '2015-02-12 23:00:17', 1, 0, 99, 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36', '1', NULL),
(10, 'Guest', '54dd17a1b5e659.97222372', 'Administrator', 1, '2015-02-12 23:13:58', '2015-02-13 23:59:39', 3846, 3, 15103112, '192.168.1.68', 'http://192.168.1.68/voitionog/chat.php', 0, 'en', '0000-00-00 00:00:00', '2015-02-13 23:59:40', 0, 0, 0, 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36', '0', NULL),
(11, 'Guest', '54dd24c3d2b280.36246562', 'Administrator', 1, '2015-02-13 00:10:26', '2015-02-13 23:11:44', 2839, 3, 14144709, '192.168.1.64', 'http://192.168.1.68/voitionog/chat.php', 0, 'en', '0000-00-00 00:00:00', '2015-02-13 23:11:45', 0, 0, 118, 'Mozilla/5.0 (compatible; MSIE 9.0; Windows Phone OS 7.5; Trident/5.0; IEMobile/9.0; NOKIA; Lumia 710)', '2', NULL),
(12, 'Guest', '54de740b480409.49651846', 'Administrator', 1, '2015-02-14 00:00:25', '2015-02-14 00:30:11', 4684, 3, 13486197, '::1', 'http://localhost/voitionog/chat.php', 0, 'en', '2015-02-14 00:30:09', '2015-02-14 00:30:12', 0, 0, 0, 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36', '0', NULL),
(13, 'Susan', '54de740b480409.49651846', 'Administrator', 1, '2015-02-14 00:30:34', '2015-02-14 00:33:26', 4775, 3, 7589341, '::1', 'http://localhost/voitionog/chat.php', 0, 'en', '2015-02-14 00:33:28', '2015-02-14 00:33:32', 0, 0, 0, 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36', '0', NULL),
(14, 'Susan', '54de740b480409.49651846', 'Doctor Macheka', 3, '2015-02-14 00:33:46', '2015-02-14 00:54:11', 5445, 3, 1521467, '::1', 'http://localhost/voitionog/chat.php', 0, 'en', '2015-02-14 00:54:29', '2015-02-14 00:54:11', 0, 0, 150, 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36', '2', NULL),
(15, 'Susan', '54de740b480409.49651846', 'Administrator', 1, '2015-02-14 00:54:58', '2015-02-14 01:19:19', 5722, 3, 13834972, '::1', 'http://localhost/voitionog/chat.php', 0, 'en', '0000-00-00 00:00:00', '2015-02-14 01:20:28', 0, 0, 0, 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36', '0', NULL),
(16, 'Susan', '54de740b480409.49651846', NULL, 0, '2015-02-14 01:19:45', '2015-02-14 01:19:54', 5731, 3, 4301446, '::1', 'http://localhost/voitionog/chat.php', 0, 'en', '2015-02-14 01:19:56', '0000-00-00 00:00:00', 0, 0, 0, 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36', '0', NULL),
(17, 'Guest', '54de740b480409.49651846', 'Doctor Macheka', 3, '2015-02-14 01:20:39', '2015-02-14 01:20:48', 5746, 2, 3021636, '::1', 'http://localhost/voitionog/chat.php', 0, 'en', '0000-00-00 00:00:00', '2015-02-14 09:57:25', 0, 0, 0, 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36', NULL, NULL),
(18, 'Guest', '54de740b480409.49651846', 'Administrator', 1, '2015-02-14 09:57:38', '2015-02-14 10:25:40', 6484, 2, 11017306, '::1', 'http://localhost/voitionog/chat.php', 0, 'en', '0000-00-00 00:00:00', '2015-02-14 10:33:51', 0, 0, 0, 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36', NULL, NULL),
(19, 'Guest', '54df0508604b42.78570418', 'Doctor Macheka', 3, '2015-02-14 10:19:08', '2015-02-14 10:20:14', 6454, 2, 7444338, '192.168.1.70', 'http://192.168.1.68/voitionog/chat.php', 0, 'en', '2015-02-14 10:33:48', '2015-02-14 10:33:50', 0, 0, 187, 'Mozilla/5.0 (Linux; Android 4.2.2; GT-P5100 Build/JDQ39) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.109 Safari/537.36', NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
