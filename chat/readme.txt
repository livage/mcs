phpMobilizer 
Copyright 2010 Andrew Rinderknecht
www.awrsolutions.com
code.google.com/p/phpmobilizer


This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.


What is it?
-=-=-=-=-=-=-=-=-=-=-=-=-=-
Automatically convert your existing website to a mobile website. phpMobilizer operates similar to many of the mobile website converters available, but it runs on your own server. 

The goal of the project is to make it as universal as possible. Just drop the files into a subdomain for automatic conversion. Add just one line of PHP to your main site if you want automatic redirection to the mobile site when someone accesses it with a phone or mobile device.


Requirements
-=-=-=-=-=-=-=-=-=-=-=-=-=-
PHP 5
Apache (.htaccess/mod_rewrite capable).


Installation instructions
-=-=-=-=-=-=-=-=-=-=-=-=-=-
More details are available on the project page at http://code.google.com/p/phpmobilizer/

Create a subdomain called 'm' for your website. 
Upload the files in the m folder to the m subdomain.
If you would like to automaticaly redirect users to the mobile site if they are accessing it with their phone, add the following to a main include file or your index.php file on your main site.

require_once('/home/path/to/subdomain/m/mobilize.php');



php-mobile-detect 
-=-=-=-=-=-=-=-=-=-=-=-=-=-
This project uses the php-mobile-detect project for mobile browser detection. 
For more information, see: http://code.google.com/p/php-mobile-detect/


